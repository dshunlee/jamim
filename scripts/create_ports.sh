#!/usr/bin/env bash
python3 -m visdom.server -port 8091 &
python3 -m visdom.server -port 8092 &
python3 -m visdom.server -port 8093 &
python3 -m visdom.server -port 8094 &
python3 -m visdom.server -port 8095 &
python3 -m visdom.server -port 8096 &
python3 -m visdom.server -port 8097 &
python3 -m visdom.server -port 8098 &
python3 -m visdom.server -port 8099 &
