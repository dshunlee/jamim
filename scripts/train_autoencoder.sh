#!/usr/bin/env bash

python3 ./code/train_autoencoder.py \
--lr_policy exp \
--encoder_hidden 128 64 64 \
--max_Freq 1500 \
--aemodel mlp \
--n_airports 60 \
--name autoencoder \
--dataroot ./data/freq_8carriers/ae_train.tfrecords \
--model ego_autoencoder \
--n_epoch 10000 \
--batchSize 64  \
--lr 0.0001 \
--suffix {encoder_hidden}_{max_Freq} \
--print_freq 6 \
--display_freq 50 \
--display_port 8098