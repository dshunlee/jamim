#!/usr/bin/env bash
# change this to the gpu device
GPU_ID=0

PRETRAIN_MODEL="./checkpoints/reg_r10_a60r10c8_6+6_drop-0.25_G-True_E-False_zeroout_0.0_2048_exp_0.0001/latest.ckpt"
DEMAND_FILE="./data/features/r10a60c8_cost_demand.npz"
DATA_ROOT="./data/features/r10a60c8_optim.tfrecords "
FREQ_MASK="./data/features/r10a60c8_train-freq.npz"
REGRESSOR="r10a60c8"
DISPLAYPORT=8099
NROUTE=10 # number of routes, keep it consistent with the pretrained model/data root/demand file/...
BUDGET=11000.0 # set budget
AIRLINE=0  # airline to be optimized

INIT_FREQ_FILE="./data/features/optim/classic_features_2009_12.npz"

for m in relu  # to specify the optimization method to be used, relu and lagrange supported
do

    CUDA_VISIBLE_DEVICES=${GPU_ID} python3 ./code/train_regressor.py \
    --what optimize \
    --save_epoch_freq 300 \
    --name opt \
    --model jamim_regression \
    --opt_airline ${AIRLINE} \
    --display_port ${DISPLAYPORT} \
    --dataroot ${DATA_ROOT} \
    --n_epoch 200 \
    --n_routes ${NROUTE} \
    --n_features 6 \
    --n_airlines 8 \
    --lr 10 \
    --suffix {opt_airline}_{n_routes}_{init_freq}_{gr_step}_originit:{init_freq}_{lr_policy}_{lr}_${REGRESSOR} \
    --fc_nfeats 6 6 \
    --use_graph_feat \
    --init_freq_file ${INIT_FREQ_FILE} \
    --cost_demand ${DEMAND_FILE} \
    --pretrained_weights ${PRETRAIN_MODEL} \
    --cost_scale 0.0002 \
    --demand_scale 0.005 \
    --over_budget_penalty 10.0 \
    --budget_penalty_beta0 1000 \
    --total_budget ${BUDGET} \
    --print_freq 10 \
    --freq_mask_file ${FREQ_MASK} \
    --lr_policy custom \
    --jamim_lr_decay 1.0 \
    --limit_max_freq \
    --optim_method ${m} \
    --budget_normalize \
    --init_freq

done
