#!/usr/bin/env bash

# change this to the gpu device
GPU_ID=0

# display and saving settings
PRIINT_FREQ=10
SAVE_FREQ=50
TEST_FREQ=5

DATA_ROOT="data/features/r10a60c8_train.tfrecords"
# change this to change the autoencoder
FREQ_MASK="./data/features/r10a60c8_train-freq.npz"


CUDA_VISIBLE_DEVICES=${GPU_ID} python3 ./code/train_regressor.py \
--name reg_r10 \
--what train \
--model jamim_regression \
--print_freq ${PRIINT_FREQ} \
--save_epoch_freq ${SAVE_FREQ} \
--test_epoch_freq ${TEST_FREQ} \
--display_port 8099 \
--dataroot ${DATA_ROOT} \
--n_routes 10 \
--n_features 6 \
--n_airlines 8 \
--n_epoch 200 \
--batchSize 2048 \
--lr 0.0001 \
--lr_policy exp \
--suffix a60r{n_routes}c{n_airlines}_{fc_nfeats}_drop-{drop_ratio}_G-{use_graph_feat}_E-{use_autoencoder}_{loss_weight_method}_{loss_weight}_{batchSize}_{lr_policy}_{lr} \
--freq_files ./data/features/filelist.txt \
--use_graph_feat \
--drop_ratio 0.25 \
--fc_nfeats 6 6 \
#--freq_mask_file ${FREQ_MASK} \
#--loss_weight 100 \
#--loss_weight_method zeroout \

#--fc_nfeats 6 6 \
#--use_autoencoder