#!/usr/bin/env bash

scripts/whole_set_r10/prep_data.sh
scripts/whole_set_r10/train_regression.sh

scripts/whole_set_r10/optimization.sh
scripts/whole_set_r10/optimization_baseline.sh
