#!/usr/bin/env bash
# change this to the gpu device
GPU_ID=0

INIT_FREQ_FILE="./data/features/optim/classic_features_2009_12.npz"
DEMAND_FILE="./data/features/r1000a60c8_cost_demand.npz"
REGRESSOR="r1000a60c8"


CUDA_VISIBLE_DEVICES=${GPU_ID} python3 ./code/train_regressor.py \
--what optimize \
--name opt \
--model jamim_regression \
--display_port 8098 \
--dataroot ./data/features/r1000a60c8_optim.tfrecords  \
--n_epoch 200 \
--n_routes 1000 \
--n_features 6 \
--n_airlines 8 \
--lr 10.0 \
--suffix {optim_method}_{total_budget}_{cost_scale}_{demand_scale}_{over_budget_penalty}_originit:{init_freq}_{lr_policy}_{lr}_${REGRESSOR} \
--fc_nfeats 6 6 \
--aemodel mlp \
--init_freq_file ${INIT_FREQ_FILE} \
--cost_demand ${DEMAND_FILE} \
--pretrained_weights "./checkpoints/reg_a60r1000c8_6+6_drop-0.0_G-True_E-False-mlp-128+64+64_zeroout_100.0_2048_exp_0.0001/latest.ckpt" \
--cost_scale 0.0002 \
--demand_scale 0.005 \
--over_budget_penalty 1.0 \
--total_budget 1060000.0 \
--print_freq 1 \
--budget_penalty_beta0 1000 \
--freq_mask_file ./data/features/r1000a60c8_train-freq.npz \
--lr_policy custom \
--limit_max_freq \
--jamim_lr_decay 1.0 \
--optim_method lagrange \
--inverse_img \
--use_graph_feat \
--budget_normalize \
#--use_autoencoder \
#--init_freq \

