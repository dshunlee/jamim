#!/usr/bin/env bash
# change this to the gpu device
GPU_ID=1

PRETRAIN_MODEL="./checkpoints/reg_a60r1000c8_6+6_drop-0.0_G-True_E-False-mlp-128+64+64_zeroout_100.0_2048_exp_0.0001/latest.ckpt"
DEMAND_FILE="./data/features/r1000a60c8_cost_demand.npz"
DATA_ROOT="./data/features/r1000a60c8_optim.tfrecords"
FREQ_MASK="./data/features/r1000a60c8_train-freq.npz"
REGRESSOR="r1000a60c8"
DISPLAYPORT=8096
NROUTE=1000
BUDGET=1100000.0

INIT_FREQ_FILE="./data/features/optim/classic_features_2009_12_optim.npz"

for m in relu
do

    CUDA_VISIBLE_DEVICES=${GPU_ID} python3 ./code/train_regressor.py \
    --what optimize \
    --save_epoch_freq 300 \
    --name opt \
    --model jamim_regression \
    --opt_airline 0 \
    --display_port ${DISPLAYPORT} \
    --dataroot ${DATA_ROOT} \
    --n_epoch 200 \
    --n_routes ${NROUTE} \
    --n_features 6 \
    --n_airlines 8 \
    --lr 20 \
    --suffix {opt_airline}_{n_routes}_{init_freq}_{gr_step}_originit:{init_freq}_{lr_policy}_{lr}_${REGRESSOR} \
    --fc_nfeats 6 6 \
    --use_graph_feat \
    --init_freq_file ${INIT_FREQ_FILE} \
    --cost_demand ${DEMAND_FILE} \
    --pretrained_weights ${PRETRAIN_MODEL} \
    --cost_scale 0.0002 \
    --demand_scale 0.005 \
    --over_budget_penalty 1.0 \
    --budget_penalty_beta0 1000 \
    --total_budget ${BUDGET} \
    --print_freq 10 \
    --freq_mask_file ${FREQ_MASK} \
    --lr_policy custom \
    --jamim_lr_decay 1.0 \
    --limit_max_freq \
    --optim_method ${m} \
    --budget_normalize \
    --init_freq

done
