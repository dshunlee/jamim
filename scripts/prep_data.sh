#!/usr/bin/env bash

python3 ./code/prep_jamim_data.py convert -a ./data/r3.npz -i ./data/features/optim/filelist.txt -o ./data/features/r3a60c8_optim.tfrecords -n ./data/features/r3a60c8_normalize.npy
python3 ./code/prep_jamim_data.py convert -a ./data/r3.npz -i ./data/features/test/filelist.txt -o ./data/features/r3a60c8_test.tfrecords -n ./data/features/r3a60c8_normalize.npy
python3 ./code/prep_jamim_data.py convert -a ./data/r3.npz -i ./data/features/val/filelist.txt -o ./data/features/r3a60c8_val.tfrecords -n ./data/features/r3a60c8_normalize.npy
python3 ./code/prep_jamim_data.py convert -a ./data/r3.npz -i ./data/features/train/filelist.txt -o ./data/features/r3a60c8_train.tfrecords -n ./data/features/r3a60c8_normalize.npy