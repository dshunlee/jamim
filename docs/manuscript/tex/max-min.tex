\documentclass[../main.tex]{subfiles}

\begin{document}
	\section{Max-min approach}
	
	Assume the objective is $f(\mathbf{x})$ and the constraint is $g(\mathbf{x})<0$, the objective is 
	\begin{align}\label{eq.opt-obj}
	\max_\mathbf{x} \min_\lambda f(\mathbf{x}) + \lambda g(\mathbf{x}) + \beta \lambda^2
	\end{align}
	At each iteration, we will get
	\begin{align}\label{eq.opt-lambda}
	\hat{\lambda}_i = \frac{-g(\hat{\mathbf{x}}_{i-1})}{2\beta}
	\end{align}
	Substitute $\lambda$ in Eq. \ref{eq.opt-obj} with Eq. \ref{eq.opt-lambda} we will obtain
	\begin{align}
	\hat{\mathbf{x}}_i&=\max_{\mathbf{x}} f(\mathbf{x}) -\frac{g(\mathbf{x})g(\hat{\mathbf{x}}_{i-1})}{2\beta} + \frac{g(\hat{\mathbf{x}}_{i-1})^2}{4\beta}\\
	&= \hat{\mathbf{x}}_{i-1}+\gamma(f'(\hat{\mathbf{x}}_{i-1}) -\frac{g(\hat{\mathbf{x}}_{i-1})}{2\beta}\vect{g}'(\hat{\mathbf{x}}_{i-1}))\\\label{eq.opt-obj-sub}
	& = \hat{\mathbf{x}}_{i-1}+\gamma(f'(\hat{\mathbf{x}}_{i-1}) -\frac{g(\hat{\mathbf{x}}_{i-1})}{2\beta}\mathbf{c})
	\end{align}
	where $\gamma$ is the learning rate; and $\mathbf{c}$ is a vector of the costs.
	
	In our case $g$ is a linear function and $g'$ is fixed. When $g(\hat{\mathbf{x}}_{i-1})<<0$ it add to the gradient descent direction an additional vector that increases the cost by increasing all of the frequencies proportional to their cost. When $g(\hat{\mathbf{x}}_{i-1})==0$, it is equivalent to the original problem. When $g(\hat{\mathbf{x}}_{i-1})>>0$, it decrease all of the frequencies proportional to their cost.
	
	For the ecample given in \ref{fig:demo-opt}, the final result may go to either left side or right side, depends on $\beta$. If $\beta$ is small, gradient descent is dominated by $g'$ (red arrow). Otherwise, it is dominated by $f'$ (blue arrow). Eventually, it will stop at some point where 
	\begin{align}\label{eq:opt-converge}
	\mathbf{f}'-\frac{g}{2\beta} \vect{g}'=0
	\end{align}.
	
	\begin{figure}
		\begin{tikzpicture}
		
		\begin{axis}[
		xmin=-5, xmax=5,
		ymin=-5, ymax=5
		]
		\addplot [name path=plot1, ultra thin, domain=-5:5, color=blue, samples=150]{x^3-5*x};
		\addplot [name path=plot2, ultra thin, domain=-5:5, color=red]{1-0.5*x};
		\addplot [name path=plot3, ultra thin, domain=-5:5]{0};
		\legend{$f(x)=3x^3-5x$,$g(x)=1-0.5x$};
		
		\addplot[mark options={blue,solid}, color=blue,mark=*,mark size=1pt]coordinates {(0.5,0.5^3-2.5)};
		\node at (axis cs:0.5,-3) [anchor=south] {$x_{i-1}$};
		\node[anchor=center] (source) at (axis cs:0.5,0.5^3-2.5){};
		\node[anchor=center](destination) at (axis cs:0,0.5^3-2.5-0.5*(3*0.5^2-5){};
		\draw[->,color=blue](source)--(destination);
		\node[anchor=center] (source1) at (axis cs:0.5,0.5^3-2.5){};
		\node[anchor=center](destination1) at (axis cs:1.5,0.5^3-2.5-0.5){};
		\draw[->,color=red](source1)--(destination1);
		\end{axis}
		\end{tikzpicture}
		\caption{Illustration of the Max-min approach.}\label{fig:demo-opt}
	\end{figure}
	
	\subsection{$\beta$ selection}
	
	Typically, the influence increases if the frequency $ \vect{x}$ increases. $\beta$ needs to be selected such that the $f_{freq}$ are updated to reduce the cost once the total cost exceed the budget in the previous iteration. This requires
	\begin{align} \label{eq:opt-over-budget}
	( \vect{f}'-\frac{g}{2\beta}  \vect{g}')\cdot  \vect{g}' < 0
	\end{align}.
	
%	For a selected $\vect{x_b}$, if $g(\vect{x_b})<0$ and Eq. \ref{eq:opt-over-budget} is satisfied, the solution will be bounded by $\vect{x_b}$ with a sufficiently small learning rate. An demonstration is presented in Figure .
	
	\begin{figure}
		\begin{subfigure}[b]{1\columnwidth}
			\includegraphics[width=1\columnwidth]{images/opt_demo}
			\caption{Gradient descent of a demo problem with varying initializations, $\beta$s and learning rates. Note how they behave differently with the same initialization but different $\beta$s.}
		\end{subfigure}
		\begin{subfigure}[b]{1\columnwidth}
			\includegraphics[width=1\columnwidth]{images/opt_grad}
			\caption{The theoretical point of converging based on Eq. \ref{eq:opt-converge}. With $x$ initialized at 1, the initial gradient may be negative or positive depends on the choice of $\beta$. }
		\end{subfigure}
		\caption{Demonstration of the effect of $beta$.}
	\end{figure}  
	
	To ensure the convergence, an idea $\beta^*$ can be selected using the following criterion presented in Eq. \ref{eq:opt-beta-idea}. The idea $\beta^*$ ensures that Eq. \ref{eq:opt-over-budget} is satisfied for all of the $\vect{x}$s that exceed the budget.
	\begin{align}\label{eq:opt-beta-idea}
	\beta^* < \frac{g(\vect{x}_e) \vect{g}'(\vect{x}_e)^T \vect{g}'(\vect{x}_e)}{2  \vect{g}'(\vect{x}_e)^T \vect{f}'(\vect{x}_e)}  \qquad \forall \vect{x}_e|g(\vect{x}_e)<0 
	\end{align}
	
	In practice, we can decrease the $\beta$ and reduce learning rate accordingly at the current $\vect{x}_i$ when $g(\vect{x}_i<0)$. 
	\begin{align}\label{eq:opt-beta}
	\beta = \frac{g(\vect{x}_e) \vect{g}'(\vect{x}_e)^T \vect{g}'(\vect{x}_e)}{2  \vect{g}'(\vect{x}_e)^T \vect{f}'(\vect{x}_i)} - \delta 
	\end{align}
	
	An adaptive gradient descent algorithm for demonstration is presented in Algorithm. \ref{alg:adaptive-gd}. The result is presented in Figure. .
	
	\begin{algorithm}
		\SetAlgoLined
		\caption{Adaptive gradient descent}\label{alg:adaptive-gd}
		\KwIn{$x_0$, $\beta$, $\beta_{min}$, $\delta$, $\gamma$, $\gamma_{min}$, $N$}
		\KwOut{$\hat{x}$}
		$x \gets x_0$\;
		\For {$i \gets 1$ to $N_a$}{ 
			$x \gets x+\gamma (f'(x)-\frac{g(x)g'(x)}{2\beta})$\tcc*[r]{Eq. \ref{eq.opt-obj-sub}}
			\If{$g(x)<0$}{
				$\beta \gets \frac{g(x)g'(x)g'(x)}{2g'(x)f'(x)} - \delta$ \tcc*[r]{Eq. \ref{eq:opt-beta}}
				\If{$\beta<0$}{
					$\beta \gets \beta_{min}$
				}
				$\gamma \gets \gamma_{min}$	
			}
		}
		$\hat{x} \gets x$ 
	\end{algorithm}
	
	\begin{figure}
		\includegraphics[width=1\columnwidth]{images/opt_demo_adaptive}
		\caption{Gradient descent of with adaptive $\beta$ and learning rate. Both the convergence and the accuracy are improved.}
		
		\caption{Demonstration of the effect of $\beta$.}
	\end{figure}  
\end{document}