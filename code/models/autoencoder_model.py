import tensorflow as tf
from .base_model import BaseModel
from . import networks


class AutoencoderModel(BaseModel):
    # modify parser to add command line options,
    # and also change the default values if needed
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        parser.add_argument('--netE', type=str, default='gcn_ae', help='selects model to use for the encoder')
        parser.add_argument('--norm_E', type=str, default='batch', help='normalization: instance | batch')
        parser.add_argument('--nl_E', type=str, default='relu', help='non-linearity: relu | lrelu')
        parser.add_argument('--netD', type=str, default='gcn_ae', help='selects model to use for the decoder')
        parser.add_argument('--norm_D', type=str, default='batch', help='normalization: instance | batch')
        parser.add_argument('--nl_D', type=str, default='lrelu', help='non-linearity: relu | lrelu')
        return parser

    def name(self):
        return 'Autoencoder'

    # initialize the network based on the options
    def initialization(self, opt, **kwargs):
        BaseModel.initialization(self, opt, **kwargs)
        self.encoder = None
        self.decoder = None
        self.z_mean = None
        self.z_log_std = None

    def custom_network(self, inputs, train_phase=True, **kwargs):
        self.z_mean = self.custom_encoder(inputs, train_phase, **kwargs)
        self.recv = self.custom_decoder(train_phase, **kwargs)
        self.loss_cost = self.custom_cost(inputs, **kwargs)

        # for testing
        if self.encoder is None:
            self.encoder = networks.define_encoder_4test(inputs)
        if self.decoder is None:
            self.decoder = networks.define_decoder_4test(self.encoder)

        return None

    # define encoder
    def custom_encoder(self, inputs, **kwargs):
        return None

    def prepare_z(self):
        if self.opt.netD.endswith('_ae'):
            z = self.z_mean
        elif self.opt.netD.endswith('_vae'):
            z = self.z_mean + tf.random_normal(self.z_mean.shape) * tf.exp(self.z_log_std)
        else:
            raise NotImplementedError('[{}] model is not implemented!'.format(self.opt.model))

        return z

    # define decoder
    def custom_decoder(self, **kwargs):
        return None

    def custom_cost(self, inputs, **kwargs):
        self.loss_cost = tf.reduce_mean(tf.square(self.decoder - placeholders))
        return self.loss_cost