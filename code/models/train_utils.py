import tensorflow as tf
import numpy as np
from tensorflow.python.client import timeline
from tensorflow.python.profiler import model_analyzer
from tensorflow.python.profiler import option_builder
import time, os


def train(opt, model, train_dataset, val_dataset=None, test_dataset=None, train_dataset_one_epoch=None,
          visualizer=None, **kwargs):

    # handle constructions. Handle allows us to feed data from different dataset by providing a parameter in feed_dict
    handle = tf.placeholder(tf.string, shape=[])
    train_phase = tf.placeholder(tf.bool, name="is_training")
    iterator = tf.data.Iterator.from_string_handle(handle, train_dataset.output_types,
                                                   train_dataset.output_shapes)
    next_batch = iterator.get_next()

    # create training data iterator
    training_iterator = train_dataset.make_one_shot_iterator()

    feed_dict = {train_phase: opt.isTrain}
    # create model
    opt.isTrain = train_phase
    model.initialization(opt, **kwargs)
    drop_ratio = None
    if opt.drop_ratio is not None:
        drop_ratio = tf.placeholder_with_default(opt.drop_ratio, shape=())
        feed_dict[drop_ratio] = opt.drop_ratio

    if opt.lr_policy == 'exp':
        global_step = tf.Variable(0, trainable=False, name='global_step')
    elif opt.lr_policy == 'custom':
        learning_rate = tf.Variable(opt.lr, dtype=tf.float32, trainable=False, name="learning_rate")

    train_var_list = model.custom_network(next_batch, train_phase=train_phase, dropout=drop_ratio)

    cost_ops = model.get_costs_ops()
    vis_ops = model.get_vis_ops()

    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    with tf.control_dependencies(update_ops):
        # create train operations
        if opt.lr_policy == 'exp':
            learning_rate = tf.train.exponential_decay(opt.lr, global_step,
                                                       100, 0.96, staircase=True)
            train_op = tf.train.AdamOptimizer(learning_rate).minimize(model.loss_cost, global_step=global_step, var_list=train_var_list)
        elif opt.lr_policy == 'custom':
            train_op = tf.train.AdamOptimizer(learning_rate).minimize(model.loss_cost, var_list=train_var_list)
        else:
            learning_rate = opt.lr
            train_op = tf.train.AdamOptimizer(learning_rate).minimize(model.loss_cost, var_list=train_var_list)

    saver = tf.train.Saver(model.get_save_varlist(), max_to_keep=opt.n_epoch)

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    # start training
    with tf.Session(config=config) as sess:
        # get_mem_summary(sess, train_op, handle, train_phase, train_dataset, model.save_dir, drop_ratio)
        sess.run(tf.global_variables_initializer())
        model.load_weights(sess)

        # get number of batches in one epoch
        nbatches = opt.n_batches
        if nbatches is None:
            nbatches = get_num_batch(sess, train_dataset_one_epoch, handle)
        print("# number of batches: {}".format(nbatches))

        # feed training data to handle
        training_handle = sess.run(training_iterator.string_handle())
        feed_dict[handle] = training_handle

        # best_result [loss, influence, over_budget, time]
        niter, total_steps, total_time, best_result, best_freq = 0, 0, 0, [0, 0, 0, 0], None
        for epoch in range(opt.n_epoch):
            #  add training code
            for i in range(nbatches):
                total_steps += opt.batchSize
                start_time = time.time()
                values = sess.run([cost_ops, vis_ops, train_op], feed_dict=feed_dict)
                end_time = time.time()
                total_time += end_time - start_time

                if model.opt.what == "optimize":
                    if best_result[1] < values[0][1] and values[0][2] < 0:
                        best_result[0] = values[0][0]
                        best_result[1] = values[0][1]
                        best_result[2] = values[0][2]
                        best_result[3] = total_time
                        best_freq = values[1][2]

                print("epoch {}, iter {}, loss: {}, time: {:.4f} s".format(epoch, i, values[0], (time.time() - start_time)))
                cur_losses = {}
                if opt.display_id > 0 and (niter+1) % opt.print_freq == 0:
                    loss_name = "train"
                    for k in range(len(model.loss_names)):
                        loss_namek = loss_name + "_" + model.loss_names[k]
                        cur_losses[loss_name] = values[0][k]
                        visualizer.plot_current_losses(epoch, float(i)/nbatches, {loss_namek: values[0][k]})
                    visualizer.print_current_losses(epoch + 1, i, cur_losses, (end_time - start_time), 0)
                niter += 1

            if opt.display_id > 0 and (epoch+1) % opt.display_freq == 0:
                save_result = total_steps % opt.update_html_freq == 0
                model.compute_visuals(vis=values[1], shape=(512, 512), interp='nearest', inverse=opt.inverse_img)
                visualizer.display_current_results(model.get_current_visuals(), epoch, save_result)

            if (epoch+1) % opt.test_epoch_freq == 0:
                if val_dataset is not None:
                    print("# On evaluation dataset!")
                    cur_losses = {}
                    eval_losses = evaluate(sess, model, handle, train_phase, val_dataset, drop_ratio, "eval_{}".format(epoch + 1))
                    loss_name = "val"
                    cur_losses = plot_losses(eval_losses, loss_name, cur_losses, opt, visualizer, epoch + 1)
                    visualizer.print_current_losses(epoch + 1, 0, cur_losses, 0, 0)

                if test_dataset is not None:
                    print("# On test dataset!")
                    cur_losses = {}
                    eval_losses = evaluate(sess, model, handle, train_phase, test_dataset, drop_ratio, "test_{}".format(epoch + 1))
                    loss_name = "test"
                    cur_losses = plot_losses(eval_losses, loss_name, cur_losses, opt, visualizer, epoch + 1)
                    visualizer.print_current_losses(epoch + 1, 0, cur_losses, 0, 0)

            if (epoch + 1) % opt.save_epoch_freq == 0:
                save_path = saver.save(sess, os.path.join(model.save_dir, "{}.ckpt".format(epoch+1)))
                print("# Model saved in path: %s" % save_path)
                save_path = saver.save(sess, os.path.join(model.save_dir, "latest.ckpt"))
                print("# Model saved in path: %s" % save_path)

            if model.opt.what == "optimize":
                if opt.loss_thresh is not None and np.abs(best_result[2]) < opt.loss_thresh:
                    break
        if model.opt.what == "optimize":
            write_to_file(model, best_result, total_time)
            np.save(os.path.join(model.save_dir, 'optim-freq.npy'), best_freq)
        save_path = saver.save(sess, os.path.join(model.save_dir, "latest.ckpt"))
        print('Latest model saved at (epoch {:d}, total_steps {:d}) to {}'.format(epoch+1, opt.n_epoch, save_path))


def eval(opt, model, train_dataset=None, val_dataset=None, test_dataset=None, **kwargs):
    datasets = {'train': train_dataset, 'val': val_dataset, 'test': test_dataset}
    handle = tf.placeholder(tf.string, shape=[])
    iterator = tf.data.Iterator.from_string_handle(handle, test_dataset.output_types,
                                                   test_dataset.output_shapes)
    next_batch = iterator.get_next()

    isTrain = tf.placeholder(tf.bool, name="is_training")
    drop_ratio = None
    if opt.drop_ratio is not None:
        drop_ratio = tf.placeholder_with_default(opt.drop_ratio, shape=())
    model.initialization(opt, **kwargs)
    model.custom_network(next_batch, train_phase=isTrain, dropout=drop_ratio)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        model.load_weights(sess)
        for name, dataset in datasets.items():
            evaluate(sess, model, handle, isTrain, dataset, drop_ratio, name+"_latest")


def get_num_batch(sess, dataset, handle):
    training_iterator = dataset.make_one_shot_iterator()
    training_handle = sess.run(training_iterator.string_handle())
    iter = dataset.make_one_shot_iterator()
    next = iter.get_next()
    nbatches = 0
    print('# Caculate number of batches!')
    while True:
        try:
            sess.run(next, feed_dict={handle: training_handle})
            # sess.run(next)
            nbatches += 1
        except tf.errors.OutOfRangeError:
            break
    return nbatches


def evaluate(sess, model, handle, isTrain, dataset, drop_ratio=None, name=None):
    val_iter = dataset.make_one_shot_iterator()
    val_handle = sess.run(val_iter.string_handle())
    feed_dict = {handle: val_handle}
    feed_dict[isTrain] = False
    if drop_ratio is not None:
        feed_dict[drop_ratio] = 0.0

    nbatches = 0
    model.init_stats()
    while True:
        try:
            values = sess.run(model.get_evaluation_ops(), feed_dict=feed_dict)
            model.collect_stats(values)
            nbatches += 1
        except tf.errors.OutOfRangeError:
            break
    losses = model.display_stats(name)

    return losses


def plot_losses(eval_losses, loss_name, cur_losses, opt, visualizer, epoch):
    disp_loss = {}
    for key, val in eval_losses.items():
        loss_namei = loss_name + '_{}'.format(key)
        cur_losses[loss_namei] = val
        disp_loss[loss_namei] = val

    if opt.display_id > 0 and visualizer is not None:
        visualizer.plot_current_losses(epoch, 0, disp_loss)

    return cur_losses


def get_mem_summary_0(sess, train_ops, handle, isTrain, dataset, save_dir, drop_ratio=None):
    run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
    run_metadata = tf.RunMetadata()

    val_iter = dataset.make_one_shot_iterator()
    val_handle = sess.run(val_iter.string_handle())
    feed_dict = {handle: val_handle}
    feed_dict[isTrain] = False
    if drop_ratio is not None:
        feed_dict[drop_ratio] = 0.0

    _ = sess.run([train_ops],
                          feed_dict=feed_dict,
                          options=run_options,
                          run_metadata=run_metadata)
    train_writer = tf.summary.FileWriter(save_dir + '/summary', sess.graph)
    train_writer.add_run_metadata(run_metadata, 'mem_test')
    print('Adding run metadata for memory test.')
    tl = timeline.Timeline(run_metadata.step_stats)
    chrome_trace = tl.generate_chrome_trace_format(show_memory=True)
    with open(save_dir + "/summary/timeline", 'w') as f:
        f.write(chrome_trace)
    print('memory test done!')


def get_mem_summary(sess, train_ops, handle, isTrain, dataset, save_dir=None, drop_ratio=None, K=1, orderby='bytes'):
    val_iter = dataset.make_one_shot_iterator()
    val_handle = sess.run(val_iter.string_handle())
    feed_dict = {handle: val_handle}
    feed_dict[isTrain] = False
    if drop_ratio is not None:
        feed_dict[drop_ratio] = 0.0

    param_stats = tf.profiler.profile(tf.get_default_graph(),options=tf.profiler.ProfileOptionBuilder.trainable_variables_parameter())

    print('total_params: %d\n' % param_stats.total_parameters)

    my_profiler = model_analyzer.Profiler(graph=sess.graph)
    run_metadata = tf.RunMetadata()

    for i in range(K):
        sess.run([tf.global_variables_initializer()],
                 options=tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE), run_metadata=run_metadata)

        tl = timeline.Timeline(run_metadata.step_stats)
        chrome_trace = tl.generate_chrome_trace_format(show_memory=True)
        if not os.path.isdir(save_dir + "/summary"):
            os.mkdir(save_dir + "/summary")
        with open(save_dir + "/summary/timeline", 'w') as f:
            f.write(chrome_trace)

        my_profiler.add_step(step=i, run_meta=run_metadata)

        profile_code_builder = option_builder.ProfileOptionBuilder()
        # profile_code_builder.with_node_names(show_name_regexes=['main.*'])
        profile_code_builder.with_min_execution_time(min_micros=15)
        profile_code_builder.select(['bytes', 'micro'])  # can be changed to 'bytes', 'occurrence'
        profile_code_builder.order_by(orderby)
        profile_code_builder.with_max_depth(15)
        my_profiler.profile_python(profile_code_builder.build())
        my_profiler.profile_operations(profile_code_builder.build())
        my_profiler.profile_name_scope(profile_code_builder.build())
        my_profiler.profile_graph(profile_code_builder.build())
        my_profiler.advise(options=model_analyzer.ALL_ADVICE)


def write_to_file(model, values, total_time):
    # best_result [loss, influence, over_budget, time]
    import csv
    filename = './data/optim_result.csv'
    if not os.path.isfile(filename):
        writer = csv.writer(open(filename, 'w', newline=''))
        writer.writerow(['type', 'model_name', 'optim_method', 'opt_airline', 'nroutes', 'init_freq',
                         'gr_step', 'total_budget', 'influence', 'overbudget', 'time_get_best_result',
                         'total_time'])
    writer = csv.writer(open(filename, 'a', newline=''))
    res_summary = []
    if model.opt.baseline is not None:
        type = model.opt.baseline
    else:
        type = 'opt'
    try:
        res_summary.extend([type, model.opt.name, model.opt.optim_method, model.opt.opt_airline, model.opt.n_routes,
                            model.opt.init_freq, model.opt.gr_step, model.opt.total_budget.eval()/model.opt.cost_scale,
                            values[1]/model.opt.demand_scale, values[2],
                            values[3], total_time])
    except AttributeError:
        res_summary.extend([type, model.opt.name, model.opt.optim_method, model.opt.opt_airline, model.opt.n_routes,
                            model.opt.init_freq, model.opt.gr_step, model.opt.total_budget/model.opt.cost_scale,
                            values[1]/model.opt.demand_scale, values[2],
                            values[3], total_time])

    writer.writerow(res_summary)
