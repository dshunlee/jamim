from .autoencoder_model import AutoencoderModel
from .gae.layers import GraphConvolution, GraphConvolutionSparse, InnerProductDecoder
import tensorflow as tf

class GraphAutoencoderModel(AutoencoderModel):
    # modify parser to add command line options,
    # and also change the default values if needed
    @staticmethod
    def modify_commandline_options(parser, is_train):
        AutoencoderModel.modify_commandline_options(parser, is_train)
        parser.add_argument('--max_Freq', type=int, default=0, help='maximum frequency')
        parser.add_argument('--max_dFreq', type=int, default=0, help='maximum frequency perturbation for data augmentation')
        parser.add_argument('--min_dFreq', type=int, default=0, help='minimum frequency perturbation for data augmentation')
        parser.add_argument('--n_airport', type=int, default=25, help='number of airports')
        parser.add_argument('--gcn_hidden', type=int, nargs='+', default=[32, 16], help='number of hidden units in each layer')
        parser.add_argument('--gcn_dropout', type=float, default=0, help='dropout ratio in gcn autoencoder')
        parser.add_argument('--adj_sum', type=float, default=0, help='sum of adjacency matrix')
        parser.add_argument('--sparse', action='store_true', help='adjacency matrix is sparse')
        parser.add_argument('--weighted', action='store_true', help='adjacency matrix is weighted')
        parser.add_argument('--save_dir', type=str, default='./data', help='save codes to file')
        return parser

    def name(self):
        return 'GraphAutoencoder'

    def initialization(self, opt, **kwargs):
        AutoencoderModel.initialization(self, opt, **kwargs)
        if self.opt.adj_sum == 0:
            self.opt.adj_sum = self.opt.n_airport
        self.pos_weight = float(self.opt.n_airport * self.opt.n_airport - self.opt.adj_sum) / self.opt.adj_sum
        self.norm = self.opt.n_airport * self.opt.n_airport / float((self.opt.n_airport * self.opt.n_airport - self.opt.adj_sum) * 2)


    def custom_cost(self, inputs, **kwargs):
        adj = inputs[1]
        if isinstance(adj, tf.SparseTensor):
            adj = tf.sparse_tensor_to_dense(adj, validate_indices=False)
        if self.opt.weighted:
            tf.sparse_tensor_to_dense(adj, validate_indices=False)
            weights = self.pos_weight * tf.cast(tf.greater(adj, 0), tf.float32) + tf.cast(tf.equal(adj, 0), tf.float32)
            self.cost = self.norm * tf.losses.mean_squared_error(adj, self.decoder, weights=weights)

            self.loss_cost = self.norm * tf.reduce_mean(
                tf.losses.mean_squared_error(labels=self.decoder,
                                             predictions=adj,
                                             weights=self.pos_weight))
        else:
            self.loss_cost = self.norm * tf.reduce_mean(
                tf.nn.weighted_cross_entropy_with_logits(logits=self.decoder,
                                                         targets=adj,
                                                         pos_weight=self.pos_weight))

        correct_prediction = tf.equal(tf.cast(tf.greater_equal(tf.sigmoid(self.decoder), 0.5), tf.int32),
                                           tf.cast(adj, tf.int32))
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

        return None

    def custom_encoder(self, inputs, **kwargs):
        adj = inputs[0]

        if len(inputs) == 3:
            x = inputs[2]
            features_nonzero = self.opt.features_nonzero
        else:
            x = None
            features_nonzero = self.opt.n_airport

        if 'gcn_ae' in self.opt.netE:
            if isinstance(x, tf.SparseTensor):
                self.hidden = GraphConvolutionSparse(self.opt.batchSize,
                                                     input_dim=self.opt.n_airport,
                                                     output_dim=self.opt.gcn_hidden[0],
                                                     features_nonzero=features_nonzero,
                                                     act=tf.nn.relu,
                                                     dropout=self.opt.gcn_dropout)(adj, x)
            else:
                self.hidden = GraphConvolution(self.opt.batchSize,
                                               input_dim=self.opt.n_airport,
                                               output_dim=self.opt.gcn_hidden[0],
                                               features_nonzero=features_nonzero,
                                               act=tf.nn.relu,
                                               dropout=self.opt.gcn_dropout)(adj, x)

            self.encoder = GraphConvolution(self.opt.batchSize,
                                            input_dim=self.opt.gcn_hidden[0],
                                            output_dim=self.opt.gcn_hidden[1],
                                            act=lambda x: x,
                                            dropout=self.opt.gcn_dropout)(adj, self.hidden)
            self.z_mean = self.encoder
            if self.opt.netE == 'gcn_vae':
                self.z_log_std = GraphConvolution(self.opt.batchSize,
                                                  input_dim=self.opt.gcn_hidden[0],
                                                  output_dim=self.opt.gcn_hidden[1],
                                                  act=lambda x: x,
                                                  dropout=self.opt.gcn_dropout)(adj, self.hidden)
        else:
            raise NotImplementedError('[{}] model is not implemented!'.format(self.opt.model))

        return self.encoder

    def custom_decoder(self, **kwargs):
        z = self.prepare_z()
        self.decoder = InnerProductDecoder(act=lambda x: x)(z)
        return self.decoder












