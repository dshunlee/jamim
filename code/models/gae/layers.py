from .initializations import *
import tensorflow as tf

# global unique layer ID dictionary for layer name assignment
_LAYER_UIDS = {}

def get_layer_uid(layer_name=''):
    """Helper function, assigns unique layer IDs
    """
    if layer_name not in _LAYER_UIDS:
        _LAYER_UIDS[layer_name] = 1
        return 1
    else:
        _LAYER_UIDS[layer_name] += 1
        return _LAYER_UIDS[layer_name]


def dropout_sparse(x, keep_prob, num_nonzero_elems):
    """Dropout for sparse tensors. Currently fails for very large sparse tensors (>1M elements)
    """
    noise_shape = [num_nonzero_elems]
    random_tensor = keep_prob
    random_tensor += tf.random_uniform(noise_shape)
    dropout_mask = tf.cast(tf.floor(random_tensor), dtype=tf.bool)
    pre_out = tf.sparse_retain(x, dropout_mask)
    return pre_out * (1./keep_prob)


class Layer(object):
    """Base layer class. Defines basic API for all layer objects.

    # Properties
        name: String, defines the variable scope of the layer.

    # Methods
        _call(inputs): Defines computation graph of layer
            (i.e. takes input, returns output)
        __call__(inputs): Wrapper for _call()
    """
    def __init__(self, **kwargs):
        allowed_kwargs = {'name', 'logging'}
        for kwarg in kwargs.keys():
            assert kwarg in allowed_kwargs, 'Invalid keyword argument: ' + kwarg
        name = kwargs.get('name')
        if not name:
            layer = self.__class__.__name__.lower()
            name = layer + '_' + str(get_layer_uid(layer))
        self.name = name
        self.vars = {}
        logging = kwargs.get('logging', False)
        self.logging = logging
        self.issparse = False

    def _call(self, *args):
        return None

    def __call__(self, *args):
        with tf.name_scope(self.name):
            outputs = self._call(*args)
            return outputs


class GraphConvolution(Layer):
    """Basic graph convolution layer for undirected graph without edge labels."""
    def __init__(self, batchsize, input_dim, output_dim, dropout=0., act=tf.nn.relu, **kwargs):
        super(GraphConvolution, self).__init__(**kwargs)
        with tf.variable_scope(self.name + '_vars'):
            self.vars['weights'] = weight_variable_glorot(input_dim, output_dim, name="weights")
        self.dropout = dropout
        self.act = act
        self.input_dim = input_dim
        self.batchsize = batchsize

    def _call(self, adj, x):
        AXW = []
        N = adj.dense_shape[1]
        x = tf.nn.dropout(x, 1-self.dropout)

        adjs = tf.sparse_split(sp_input=adj, num_split=self.batchsize, axis=0)
        for i in range(self.batchsize):
            adji = tf.sparse_reshape(adjs[i], [N, N])
            XWi = tf.matmul(x[i], self.vars['weights'])
            AXWi = tf.sparse_tensor_dense_matmul(adji, XWi)
            AXW += [AXWi]
        x = tf.stack(AXW)

        outputs = self.act(x)
        return outputs


def creat_sparse_eye(batch, N):
    idb = tf.reshape(tf.range(0, batch, dtype=tf.int64), [batch, 1])
    idb = tf.reshape(tf.tile(idb, [1, N]), [-1])
    idx = tf.tile(tf.range(0, N, dtype=tf.int64), [batch])

    indices = tf.transpose(tf.stack([idb, idx, idx], axis=0))
    id_mat = tf.SparseTensor(indices, tf.ones([batch*N]), tf.stack([batch, N, N]))
    return id_mat


class GraphConvolutionSparse(Layer):
    """Graph convolution layer for sparse inputs."""
    def __init__(self, batchsize, input_dim, output_dim, features_nonzero, dropout=0., act=tf.nn.relu, **kwargs):
        super(GraphConvolutionSparse, self).__init__(**kwargs)
        with tf.variable_scope(self.name + '_vars'):
            self.vars['weights'] = weight_variable_glorot(input_dim, output_dim, name="weights")
        self.dropout = dropout
        self.act = act
        self.issparse = True
        self.features_nonzero = features_nonzero
        self.input_dim = input_dim
        self.batchsize = batchsize

    def _call(self, adj, x=None):
        AXW = []
        N = adj.dense_shape[1]
        if x is None:
            x = creat_sparse_eye(self.batchsize, N)
        # tfprint = tf.print('x:\n', x)
        # tfutil.TFPRINTS.append(tfprint)
        x = dropout_sparse(x, 1 - self.dropout, self.features_nonzero)
        xs = tf.sparse_split(sp_input=x, num_split=self.batchsize, axis=0)
        adjs = tf.sparse_split(sp_input=adj, num_split=self.batchsize, axis=0)
        # tfprint = tf.print('split:\n', x.dense_shape, xs[0].dense_shape)
        # tfutil.TFPRINTS.append(tfprint)
        for i in range(self.batchsize):
            xi = tf.sparse_reshape(xs[i], [N, self.input_dim])
            adji = tf.sparse_reshape(adjs[i], [N, N])
            XWi = tf.sparse_tensor_dense_matmul(xi, self.vars['weights'])
            AXWi = tf.sparse_tensor_dense_matmul(adji, XWi)
            AXW += [AXWi]
        x = tf.stack(AXW)
        outputs = self.act(x)
        return outputs


class InnerProductDecoder(Layer):
    """Decoder model layer for link prediction."""
    def __init__(self, dropout=0., act=tf.nn.sigmoid, **kwargs):
        super(InnerProductDecoder, self).__init__(**kwargs)
        self.dropout = dropout
        self.act = act

    def _call(self, inputs):
        inputs = tf.nn.dropout(inputs, 1-self.dropout)
        x = tf.transpose(inputs, perm=[0, 2, 1])
        x = tf.matmul(inputs, x)
        outputs = self.act(x)
        return outputs


class AdjMat2Unweighted(Layer):
    def __init__(self, **kwargs):
        super(AdjMat2Unweighted, self).__init__(**kwargs)

    def _call(self, inputs):
        values = tf.ones_like(inputs.values)
        return tf.SparseTensor(inputs.indices, values, inputs.dense_shape)


class AdjMatNormalize(Layer):
    def __init__(self, **kwargs):
        super(AdjMatNormalize, self).__init__(**kwargs)

    def _call(self, inputs):
        rowsum = tf.sparse_reduce_sum(inputs, axis=-1)
        return tf.SparseTensor(inputs.indices, values, inputs.dense_shape)
