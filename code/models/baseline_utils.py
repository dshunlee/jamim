import tensorflow as tf
import os
import time
import numpy as np


def train(opt, model, optim_dataset, visualizer=None, **kwargs):
    iterator = optim_dataset.make_one_shot_iterator()
    next_batch = iterator.get_next()

    model.initialization(opt, **kwargs)

    model.custom_network(next_batch, train_phase=False, dropout=0)

    cost_ops = model.get_costs_ops()
    vis_ops = model.get_vis_ops()

    saver = tf.train.Saver(model.get_save_varlist(), max_to_keep=opt.n_epoch)

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    # start training
    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        model.load_weights(sess)
        # get number of routes for optimization
        coords = sess.run(model.route_coords)
        NR, maxinf = coords.shape[0], 0
        # get init freq as current optim
        freq_optim = sess.run(model.freq_var)

        niter, total_steps, total_time, cur_losses, best_influence = 0, 0, 0, {}, 0
        for epoch in range(opt.n_epoch):
            #  add training code
            total_steps += 1
            start_time = time.time()
            update_success = model.baseline_update_before_predict(sess)
            if not update_success:
                import csv
                filename = './data/optim_result.csv'
                if not os.path.isfile(filename):
                    writer = csv.writer(open(filename, 'w', newline=''))
                    writer.writerow(['type', 'model_name', 'optim_method', 'opt_airline', 'nroutes', 'init_freq',
                                     'gr_step', 'total_budget', 'influence', 'overbudget', 'time_get_best_result',
                                     'total_time'])
                writer = csv.writer(open(filename, 'a', newline=''))
                res_summary = []
                try:
                    res_summary.extend(
                        [model.opt.baseline, model.opt.name, model.opt.baseline, model.opt.opt_airline, model.opt.n_routes, model.opt.init_freq,
                         model.opt.gr_step,
                         model.opt.total_budget.eval()/model.opt.cost_scale, best_influence / model.opt.demand_scale,
                         best_losses[2], total_time])
                except AttributeError:
                    res_summary.extend(
                        [model.opt.baseline, model.opt.name, model.opt.baseline, model.opt.opt_airline, model.opt.n_routes, model.opt.init_freq,
                         model.opt.gr_step,
                         model.opt.total_budget/model.opt.cost_scale, best_influence / model.opt.demand_scale,
                         best_losses[2], total_time])

                writer.writerow(res_summary)
                break

            values = sess.run([cost_ops, vis_ops, model.freq_optim])
            model.baseline_update_after_predict(values)
            end_time = time.time()
            total_time += end_time - start_time

            losses = values[0]

            if (best_influence < losses[1]) and (losses[2] < 0):
                best_influence = losses[1]
                best_losses = losses
                best_freq = values[1][2]
                best_step = total_steps

            if losses[2] >= 0:
                import csv
                writer = csv.writer(open('./data/opt_new.csv', 'a', newline=''))
                res_summary = []
                try:
                    res_summary.extend(['baseline', model.opt.name, model.opt.baseline, model.opt.opt_airline, model.opt.n_routes, model.opt.init_freq, model.opt.gr_step,
                                    model.opt.total_budget.eval(), best_losses[0], best_influence/model.opt.demand_scale, best_losses[2], total_time])
                except AttributeError:
                    res_summary.extend(['baseline', model.opt.name, model.opt.baseline, model.opt.opt_airline, model.opt.n_routes, model.opt.init_freq, model.opt.gr_step,
                                    model.opt.total_budget, best_losses[0], best_influence/model.opt.demand_scale, best_losses[2], total_time])

                writer.writerow(res_summary)

                np.save(os.path.join(model.save_dir, 'optim-freq.npy'), best_freq)

                print("stop training, cost over budget! \n Best network found at {} th iteration!".format(best_step))
                break

            loss_name = "baseline"
            if (epoch+1) % opt.print_freq == 0 and opt.display_id > 0:
                print("epoch {}, loss: {}, time: {:.4f} s".format(epoch, losses, (time.time() - start_time)))
                for k in range(len(model.loss_names)):
                    loss_namek = loss_name + "_" + model.loss_names[k]
                    cur_losses[loss_name] = losses[k]
                    visualizer.plot_current_losses(epoch+1, 0, {loss_namek: losses[k]})
                visualizer.print_current_losses(epoch + 1, 0, cur_losses, (end_time - start_time), 0)

            if (epoch + 1) % opt.display_freq == 0:
                model.compute_visuals(vis=values[1], shape=(512, 512), interp='nearest', inverse=opt.inverse_img)
                visualizer.display_current_results(model.get_current_visuals(), epoch, True)

            if (epoch + 1) % opt.save_epoch_freq == 0:
                save_path = saver.save(sess, os.path.join(model.save_dir, "{}.ckpt".format(epoch+1)))
                print("# Model saved in path: %s" % save_path)
                save_path = saver.save(sess, os.path.join(model.save_dir, "latest.ckpt"))
                print("# Model saved in path: %s" % save_path)

        save_path = saver.save(sess, os.path.join(model.save_dir, "latest.ckpt"))
        print("# Model saved in path: %s" % save_path)


def plot_losses(eval_losses, loss_name, cur_losses, opt, visualizer, epoch):
    disp_loss = {}
    for key, val in eval_losses.items():
        loss_namei = loss_name + '_{}'.format(key)
        cur_losses[loss_namei] = val
        disp_loss[loss_namei] = val

    if opt.display_id > 0 and visualizer is not None:
        visualizer.plot_current_losses(epoch, 0, disp_loss)

    return cur_losses

