import tensorflow as tf
import numpy as np


class JamimBaseLayer(tf.keras.layers.Layer):
    def __init__(self, n_routes, n_airlines, output_dim, **kwargs):
        super(JamimBaseLayer, self).__init__(**kwargs)
        self.output_dim = output_dim
        self.n_airlines = n_airlines
        self.n_routes = n_routes

    def build(self, input_shape):
        self.kernels = []

    def call(self, input):
        self.__gather(input)
        return None

    def _gather(self, input):
        r_id = input[-1]  # n
        kns = []
        for kn in self.kernels:
            kns.append(tf.gather(kn, r_id))  # n x f

        return kns


class JamimFCLayer(JamimBaseLayer):
    def __init__(self, n_routes, n_airlines, n_output_features, use_bias=True, activation='relu',
                 kernel_initializer='he_uniform', bias_initializer='zeros', **kwargs):
        super(JamimFCLayer, self).__init__(n_routes, n_airlines, n_output_features, **kwargs)
        self.use_bias = use_bias
        self.activation = tf.keras.activations.get(activation)
        self.output_dim = n_airlines*n_output_features
        self.kernel_initializer = tf.keras.initializers.get(kernel_initializer)
        self.bias_initializer = tf.keras.initializers.get(bias_initializer)

    def build(self, input_shape):
        JamimBaseLayer.build(self, input_shape)
        kernel = self.add_variable("kernel", shape=[self.n_routes, self.output_dim,  input_shape[0][-1].value],
                                   initializer=self.kernel_initializer)

        self.kernels.append(kernel)

        if self.use_bias:
            self.kernels.append(self.add_variable("bias", shape=[self.n_routes, self.output_dim],
                                                  initializer=self.bias_initializer))

    def call(self, input):
        kns = self._gather(input)
        x = tf.tile(tf.expand_dims(input[0], 1), [1, self.output_dim, 1])
        out = tf.reduce_sum(x * kns[0], axis=-1)
        if self.use_bias:
            out = out + kns[1]

        if self.activation is not None:
            out = self.activation(out)
        return out


class JamimMultilogitLayer(JamimBaseLayer):
    def __init__(self, n_routes, n_airlines, kernel_initializer='he_uniform',
                 bias_initializer='zeros', uniform_max=0.05, **kwargs):
        super(JamimMultilogitLayer, self).__init__(n_routes, n_airlines, n_airlines, **kwargs)
        self.output_dim = n_airlines
        if uniform_max > 0:
            self.kernel_initializer = tf.initializers.random_uniform(-uniform_max, uniform_max)
        else:
            self.kernel_initializer = tf.keras.initializers.get(kernel_initializer)

        self.bias_initializer = tf.keras.initializers.get(bias_initializer)

    def build(self, input_shape):
        JamimBaseLayer.build(self, input_shape)
        self.kernels.append(self.add_variable("kernel", shape=[self.n_routes, input_shape[0][-1].value],
                                              initializer=self.kernel_initializer))
        self.kernels.append(self.add_variable("bias", shape=[self.n_routes, 1],
                                              initializer=self.bias_initializer))

    def call(self, input):
        feat = input[0]
        feat = tf.transpose(feat, perm=[1, 0, 2])  # n x a x f -> a x n x f

        kns = self._gather(input)

        xwi = tf.transpose(tf.multiply(feat, kns[0]), perm=[1, 0, 2])  # a x n -> n x a
        xwi = tf.reduce_sum(xwi, axis=-1)
        xwb = xwi + kns[1]

        out = tf.nn.softmax(xwb)

        return out


class ConcatFeatLayer(tf.keras.layers.Layer):
    def __init__(self, n_airlines=8, graph_feat=None, n_graph_feat=0,
                 encoder_feat=None, n_encoder_feat=0):
        super(ConcatFeatLayer, self).__init__()
        self.n_airlines = n_airlines
        self.graph_feat = graph_feat
        self.encoder_feat = encoder_feat
        self.n_graph_feat = n_graph_feat
        self.n_encoder_feat = n_encoder_feat

    def call(self, input):
        outputs = tf.py_func(self.gather_feats, [input[0], input[1]], tf.float32)
        outputs = tf.reshape(outputs, (-1, self.n_airlines, 2*self.n_graph_feat + self.n_encoder_feat))
        return outputs

    def gather_feats(self, files, coords):
        feats = []
        for i in range(files.shape[0]):
            fi = files[i].decode("utf-8")
            # read from precomputed data
            featsi = []
            if self.graph_feat is not None:
                feat_net = self.graph_feat[fi]  # N x A x F
                feat_net = np.concatenate((feat_net[coords[i, 0]], feat_net[coords[i, 1]]), -1)  # A x 2F
                featsi += [feat_net]
            if self.encoder_feat is not None:
                codes = self.encoder_feat[fi]  # A x K
                featsi += [codes]
            feats += [np.concatenate(featsi, 1)]
        return np.stack(feats)