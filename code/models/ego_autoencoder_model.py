from .autoencoder_model import AutoencoderModel
import tensorflow as tf
import numpy as np


class EgoAutoencoderModel(AutoencoderModel):
    # modify parser to add command line options,
    # and also change the default values if needed
    @staticmethod
    def modify_commandline_options(parser, is_train):
        AutoencoderModel.modify_commandline_options(parser, is_train)
        parser.add_argument('--max_Freq', type=int, default=1000, help='maximum frequency')
        parser.add_argument('--max_dFreq', type=int, default=0.01, help='maximum frequency perturbation for data augmentation')
        parser.add_argument('--min_dFreq', type=int, default=0.001, help='minimum frequency perturbation for data augmentation')
        parser.add_argument('--n_airports', type=int, default=60, help='number of airports')
        parser.add_argument('--encoder_hidden', '-eh', type=int, nargs='+', default=[128, 64, 60], help='number of hidden units in encoder')
        parser.add_argument('--decoder_hidden', '-dh', type=int, nargs='+', default=[32, 16], help='number of hidden units decoder, default revise encoder')
        parser.add_argument('--mlp_dropout', type=float, default=0, help='dropout ratio in gcn autoencoder')
        parser.add_argument('--save_dir', type=str, default='./data', help='save codes to file')
        parser.add_argument('--aemodel', type=str, default='mlp', help='dropout ratio in gcn autoencoder')
        parser.add_argument('--use_ego', action="store_true", help='use ego-network')
        return parser

    def name(self):
        return 'EgoAutoencoder'

    def initialization(self, opt, **kwargs):
        AutoencoderModel.initialization(self, opt, **kwargs)
        self.visual_ops = ['real', 'recv', 'residual']  # haven't creat graph yet, use string

    @staticmethod
    def __weighted_sparse_matrix_loss(adj, pred):
        NZ = tf.cast(tf.shape(adj)[1], tf.float32)
        mask = tf.to_float(adj > 0)
        sum = tf.reduce_sum(mask, axis=1, keep_dims=True)
        pos_w = (NZ - sum) / sum
        weights = mask * pos_w + (1 - mask)
        #norm = NZ / (2 * (NZ - sum))
        #weights = norm * weights

        #print_op = tf.print("Real: ", adj, "Fake: ", pred, "NZ: ", NZ, "sum: ", sum, "pos_w: ", pos_w, 'weights: ', weights)
        #with tf.control_dependencies([print_op]):
        loss_cost = tf.losses.mean_squared_error(labels=adj, predictions=pred, weights=weights)
        return loss_cost

    def custom_cost(self, inputs, **kwargs):
        self.real = inputs
        self.residual = tf.abs(self.recv - self.real)
        real = tf.reshape(self.real, (-1, self.opt.n_airports * self.opt.n_airports))
        recv = tf.reshape(self.recv, (-1, self.opt.n_airports * self.opt.n_airports))
        self.loss_cost = self.__weighted_sparse_matrix_loss(real, recv)
        return self.loss_cost

    def custom_encoder(self, inputs, train_phase=True, **kwargs):
        with tf.variable_scope('encoder'):
            if self.opt.aemodel == 'mlp':
                inputs = tf.reshape(inputs, (-1, self.opt.n_airports*self.opt.n_airports))
                self.encoder = self.get_mlp_model(inputs,
                                                  self.opt.encoder_hidden,
                                                  training=train_phase)
            elif self.opt.aemodel == 'gcn':
                self.encoder = self._get_gcn_encoder(inputs)
        return self.encoder

    def custom_decoder(self, train_phase=True, **kwargs):
        with tf.variable_scope('decoder'):
            z = self.prepare_z()
            if self.opt.aemodel == 'mlp':
                self.opt.decoder_hidden = self.opt.encoder_hidden[::-1]
                self.opt.decoder_hidden = self.opt.decoder_hidden[1:]
                self.opt.decoder_hidden.append(self.opt.n_airports * self.opt.n_airports)
                self.decoder = self.get_mlp_model(z,
                                                  self.opt.decoder_hidden,
                                                  last_act=tf.keras.layers.Activation('sigmoid'),
                                                  training=train_phase)
            elif self.opt.aemodel == 'gcn':
                self.decoder = self._get_gcn_decoder(z)
            self.decoder = tf.reshape(self.decoder, (-1, self.opt.n_airports, self.opt.n_airports))
        return self.decoder

    def _get_gcn_encoder(self, input):
        model = tf.keras.Sequential()
        for i in range(len(self.opt.encoder_hidden)):
            model.add(GraphConvDenseLayer(self.opt.n_airports, self.opt.encoder_hidden[i]))
            if i < len(self.opt.encoder_hidden)-1:
                model.add(tf.keras.layers.ReLU())
        model.add(tf.keras.layers.MaxPool1D(self.opt.n_airports))
        return model(input)

    def _get_gcn_decoder(self, input):
        decoder_hidden = np.array(self.opt.decoder_hidden) * self.opt.n_airports
        input = self.get_mlp_model(input, decoder_hidden[:-1])
        input = tf.layers.Dense(decoder_hidden[-1])(input)
        input = tf.reshape(input, (self.opt.n_airports, self.opt.decoder_hidden[-1]))
        output = InnerProductDenseLayer(self.opt.n_airports, self.opt.drop_ratio)(input)
        return output


class GraphConvDenseLayer(tf.keras.layers.Layer):
    def __init__(self, num_nodes, num_outputs):
        super(GraphConvDenseLayer, self).__init__()
        self.num_nodes = num_nodes
        self.num_outputs = num_outputs

    def build(self, input_shape):
        self.kernel = self.add_variable("kernel", shape=[input_shape.as_list()[-1], self.num_outputs])

    def call(self, input):
        kernel = tf.tile(tf.expand_dims(self.kernel, 0), (tf.shape(input)[0], 1, 1))
        return tf.matmul(input, kernel)

    def compute_output_shape(self, input_shape):
        shape = input_shape.as_list()
        return (shape[0], shape[1], self.num_outputs)


class InnerProductDenseLayer(tf.keras.layers.Layer):
    def __init__(self, in_dim, dropout):
        super(InnerProductDenseLayer, self).__init__()
        self.in_dim = in_dim
        self.dropout = dropout

    def build(self, input_shape):
        pass

    def call(self, inputs):
        if self.dropout > 0:
            inputs = tf.nn.dropout(inputs, 1-self.opt.drop_ratio)
        x = tf.transpose(inputs, (0, 2, 1))
        x = tf.matmul(inputs, x)
        x = tf.reshape(x, (-1, self.in_dim*self.in_dim))
        return x

    def compute_output_shape(self, input_shape):
        shape = input_shape.as_list()
        return (shape[0], shape[1]*shape[1])












