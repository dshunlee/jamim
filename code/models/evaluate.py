import numpy as np
import pickle
import matplotlib.pyplot as plt

def evl_r2_pearson(pred, label):
    # r2 = 0
    r2_by_rt = {}
    cc_by_rt = {}
    mse_by_rt = {}
    rel_err_rt = {}

    for rt in pred.keys():
        label_rt = np.array(label[rt])
        pred_rt = np.array(pred[rt])
        try:
            r2_by_rt[rt] = cal_r2(pred_rt, label_rt)
            cc_by_rt[rt] = cal_pearson(pred_rt, label_rt)
            mse_by_rt[rt] = cal_mse(pred_rt, label_rt)
            # rel_err_rt[rt] = relative_error(pred_rt, label_rt)
        except ValueError:
            print('dimension not match!')
            break
    # with open('r2.pkl', 'wb') as f:
    #     pickle.dump(r2_by_rt, f, pickle.HIGHEST_PROTOCOL)
    # with open('cc.pkl', 'wb') as f:
    #     pickle.dump(cc_by_rt, f, pickle.HIGHEST_PROTOCOL)
    r2 = sum(r2_by_rt.values())/len(r2_by_rt)
    cc = sum(cc_by_rt.values()) / len(cc_by_rt)
    mse = sum(mse_by_rt.values()) / len(mse_by_rt)
    # err = sum(rel_err_rt.values()) / len(rel_err_rt)
    with open('errors.pkl', 'wb') as fout:
        pickle.dump(r2_by_rt, fout)
        pickle.dump(cc_by_rt, fout)
        pickle.dump(mse_by_rt, fout)
    return r2, cc, mse


def relative_error(pred, label):
    err = np.abs(pred-label)
    relative_err = err/label
    return relative_err.mean(axis=1).mean()


def cal_mse(pred,label):
    err2 = (pred-label)**2
    return err2.mean(axis=1).mean()


def cal_r2(pred, label):
    mean_sample = label.mean(axis=1).reshape(-1, 1)
    sse = ((pred - label) ** 2).sum(axis=1)
    tse = ((label - mean_sample) ** 2).sum(axis=1)
    r2 = 1.0 - (sse / tse)
    return r2.mean()


def cal_pearson(x, y):
    n = x.shape[1]
    x_diff = x - x.mean(axis=1).reshape(-1, 1)
    y_diff = y - y.mean(axis=1).reshape(-1, 1)

    cov_xy = np.multiply(x_diff, y_diff).sum(axis=1)/(n-1)
    x_std = np.sqrt((x_diff**2).sum(axis=1)/(n-1))
    y_std = np.sqrt((y_diff ** 2).sum(axis=1)/(n-1))

    cc = cov_xy/(np.multiply(x_std, y_std))
    return cc.mean()