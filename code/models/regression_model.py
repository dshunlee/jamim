
import tensorflow as tf
from .base_model import BaseModel

class RegressionModel(BaseModel):
    # modify parser to add command line options,
    # and also change the default values if needed
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        return parser

    def name(self):
        return 'Regression'

    # initialize the network based on the options
    def initialization(self, opt, **kwargs):
        BaseModel.initialization(self, opt, **kwargs)
        pass

    # define regression network
    def custom_network(self, input, train_phase=True, **kwargs):
        return None
