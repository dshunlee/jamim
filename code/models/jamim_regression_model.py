from .regression_model import RegressionModel
from .jamim_layers import *
from .graph_feature_layer import *
from .ego_autoencoder_model import *
import matplotlib.pyplot as plt
import itertools
import numpy as np
import os


class JamimRegressionModel(RegressionModel):
    # modify parser to add command line options,
    # and also change the default values if needed
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        parser.add_argument('--n_routes', type=int, default=100, help='number of routes')
        parser.add_argument('--n_airlines', type=int, default=8, help='number of airlines')
        parser.add_argument('--n_airports', type=int, default=60, help='number of airports')
        parser.add_argument('--n_features', type=int, default=6, help='number of features')
        parser.add_argument('--normarlize_file', type=str, help='2 x NAL x NF matrix, mean and scale')
        parser.add_argument('--net_reg', type=str, default='reg_mlp', help='regression model')
        parser.add_argument('--fc_nfeats', type=int, nargs='+', default=[],
                            help='number of features for each airline  for each fc layers')
        parser.add_argument('--use_graph_feat', '-gf', action="store_true", help='use graph features')
        parser.add_argument('--use_autoencoder', '-ae', action="store_true", help='use autoencoder')
        parser.add_argument('--jamim_ini_max', type=float, default=0.05, help='jamim multilogit layer init bound')
        parser.add_argument('--loss_weight', type=float, default=0.0, help='weights for nonzero influence')
        parser.add_argument('--loss_weight_method', type=str, default="zeroout", help='zeroout/freq/weight')
        parser.add_argument('--freq_mask_file', type=str, help='file to freq and mask file')
        parser.add_argument('--optim_route_idx', type=int, nargs='+', default=[], help='route to optimize')

        parser.add_argument('--aemodel', type=str, default='mlp', help='dropout ratio in gcn autoencoder')
        parser.add_argument('--encoder_hidden', '-eh', type=int, nargs='+', default=[128, 64, 60],
                            help='number of hidden units in encoder')
        parser.add_argument('--max_Freq', type=int, default=0, help='maximum frequency')
        parser.add_argument('--pretrained_weights', type=str, help='pretrained autoencoder or the whole model')

        parser.add_argument('--mlp_dropout', type=float, default=0, help='dropout ratio in gcn autoencoder')
        parser.add_argument('--what', type=str, default='train', help='train/optimize')
        parser.add_argument('--optim_method', type=str, default='relu', help="optimization method, [relu | lagrange]")
        parser.add_argument('--init_freq_file', type=str, help="initial freqency file")
        parser.add_argument('--init_freq', action="store_true", help="initial freqency from file, default zeros")
        parser.add_argument('--opt_airline', type=int, default=0, help='the airline to maximize')
        parser.add_argument('--cost_demand', type=str, default=0, help='path to cost and demand file')
        parser.add_argument('--cost_scale', type=float, default=0.01, help='scale the cost')
        parser.add_argument('--demand_scale', type=float, default=0.0001, help='scale the demand')
        parser.add_argument('--total_budget', type=float, default=0.0, help='total budget')
        parser.add_argument('--over_budget_penalty', type=float, default=1.0,
                            help='over budget penalty in objective function')
        parser.add_argument('--budget_penalty_beta0', type=float,
                            help='over budget beta0 (default 0.95) for optimization')
        parser.add_argument('--budget_normalize', action="store_true", help='normalize the cost with the total budget')
        parser.add_argument('--jamim_lr_decay', type=float, help='learning rate decay for optimization')
        parser.add_argument('--freq_files', type=str, help="list of freq/feature files")
        parser.add_argument('--limit_max_freq', action="store_true", help='limit maximum freq to existing maximum')
        parser.add_argument('--max_freq_scale', type=float, default=1.0, help='limit maximum freq to existing maximum')
        parser.add_argument('--ticket_price_file', type=str, help="ticket price file")

        parser.add_argument('--baseline', type=str, help="baseline method")
        parser.add_argument('--gr_step', type=float, default=10.0, help='update step for greedy algorithm')
        parser.add_argument('--ggr_n_group', type=int, default=2, help='number of groups for group greedy algorithm')
        parser.add_argument('--ggr_budget_step', type=float, default=0.01,
                            help='number of groups for group greedy algorithm')

        parser.add_argument('--loss_thresh', type=float, help="thresh of loss to terminate the training / optimization")

        parser.add_argument('--inverse_img', action="store_true", help="inverse the image for visualization")

        return parser

    def __init__(self, **kwargs):
        self.graph_feat, self.encoder_feat = None, None
        super(JamimRegressionModel, self).__init__(**kwargs)

        self.n_graph_feat, self.n_encoder_feat = 0, 0
        if 'graph_feat' in kwargs.keys() and self.graph_feat is not None:
            self.n_graph_feat = next(iter(self.graph_feat.values())).shape[-1]
        if 'encoder_feat' in kwargs.keys() and self.encoder_feat is not None:
            self.n_encoder_feat = next(iter(self.encoder_feat.values())).shape[-1]

        self.__displayed_once = set()

    def name(self):
        return 'JamimRegression'

    def initialization(self, opt, **kwargs):
        RegressionModel.initialization(self, opt, **kwargs)
        self.embed_online = True
        if self.opt.what == "optimize":
            self.__init_4_optimization()
        elif self.opt.freq_files is not None:
            self.__init_embeding_config()

        if self.opt.what == "optimize":
            self.save_dir += "/optimize"
            if self.opt.baseline is not None:
                self.save_dir += "/baseline_{}".format(self.opt.baseline)
                self.__init_baseline()
            if not os.path.isdir(self.save_dir):
                os.makedirs(self.save_dir)

    def custom_network(self, inputs, train_phase=True, **kwargs):
        if self.opt.what == "optimize" and self.opt.baseline == "greedy_loop":  # using tensorflow loop
            pred = self.baseline_one_step_loop(inputs)
        else:
            feats = self.__cal_all_features(inputs)
            input_pred = (feats, inputs[2])
            with tf.variable_scope('regression'):
                pred = self.__predict(input_pred, train_phase, **kwargs)

        self.eval_pred, self.eval_real, self.eval_route_id = pred, inputs[1], inputs[2]
        self.eval_ops += ['eval_pred', 'eval_real', "eval_route_id"]

        self.__costum_loss(inputs[1], pred, freq=inputs[0][:, :, 0], route=inputs[2])

        # return trainable variables
        if self.opt.what == "optimize":
            trainable = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='frequency')
        else:
            trainable = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='regression')

        return trainable

    def load_weights(self, sess, **kwargs):
        if self.opt.what == "train" and self.opt.use_autoencoder:
            # load pretrained autoencoder
            self.__load_encoder(sess, **kwargs)
        elif self.opt.what == 'optimize' or self.opt.continue_train:
            self.__load_all_models(sess)

    def baseline_one_step_loop(self, inputs):
        if self.opt.baseline == "greedy_loop":
            return self.__baseline_one_step_loop_gr(inputs)

    def baseline_ggr_outer_loop(self, sess, inputs):
        max_infl = 0
        infls = []
        for grp_id in self.ggr_update_grp_idx:
            infl_i = self.__baseline_ggr_group_dp(sess, inputs, grp_id)
            infls += [infl_i]
            if infl_i > max_infl:
                max_infl = infl_i
        self.ggr_update_grp_idx = np.where(infls == max_infl)
        self.ggr_total_n += 1

    def baseline_update_before_predict(self, sess):
        if self.opt.baseline == "brutal":
            return self.__baseline_brutal_update_before_predict(sess)
        elif self.opt.baseline == "group_greedy":
            return self.__baseline_group_greedy_update_before_predict(sess)
        elif self.opt.baseline == "greedy":
            return self.__baseline_greedy_update_before_predict(sess)
        else:
            return True

    def baseline_update_after_predict(self, values):
        losses = values[0]
        freqs = values[1]

        if losses[1] > self.max_influence:
            self.max_influence = losses[1]
            self.best_freq_network_delay = freqs[1]

    def get_save_varlist(self, var_list=None):
        self.get_load_varlist(var_list)

    def get_load_varlist(self, var_list=None):
        if var_list is None:
            vars = tf.global_variables()
        else:
            vars = {}
            for var in var_list:
                vars[var.name[:-2].replace('while/', '')] = var
        return vars

    def init_stats(self):
        RegressionModel.init_stats(self)
        self.eval_stats['route-carrier-err'] = [[] for i in range(self.opt.n_routes)]
        self.eval_stats['route-carrier-hist'] = np.zeros((self.opt.n_routes, self.opt.n_airlines))
        self.err_stats = {}

    def collect_stats(self, values):
        RegressionModel.collect_stats(self, values)
        mask = values[2] > 0
        if self.opt.freq_mask_file is not None:
            data = np.load(self.opt.freq_mask_file)
            mask = data['mask'][values[3]] > 0
        err = (values[1] - values[2]) * mask  # pred - real, 12 x 8
        cnt = values[2] > 0
        for i in range(values[3].shape[0]):
            self.eval_stats['route-carrier-err'][values[3][i]].append(err[i])
            self.eval_stats['route-carrier-hist'][values[3][i]] += cnt[i]
            if values[3][i] in self.eval_stats:
                self.eval_stats[values[3][i]] += [values[1][i], values[2][i]]
            else:
                self.err_stats[values[3][i]] = [values[1][i], values[2][i]]

    def display_stats(self, name=None):
        losses = RegressionModel.display_stats(self, name)
        rc_stds = []  # R x C
        r_stds = []  # R
        for ri in self.eval_stats['route-carrier-err']:
            ri_err = np.stack(ri)  # N x C
            r_stds += [np.std(ri_err)]
            rc_stds += [np.std(ri_err, 0)]
        r_stds = np.array(r_stds)
        rc_stds = np.array(rc_stds)

        rc_stds_img = rc_stds.repeat(64, 1)
        K = 1
        if rc_stds_img.shape[0] < 526:
            K = 526 // rc_stds_img.shape[0] + 1
        rc_stds_img = rc_stds_img.repeat(K, 0)
        plt.imshow(rc_stds_img.T)
        plt.colorbar(orientation='horizontal')
        plt.savefig(os.path.join(self.save_dir, '{}_route-carrier-err.jpg'.format(name)), bbox_inches='tight', dpi=300)
        plt.close()

        r_stds_img = np.array(r_stds).reshape(-1, 1)
        r_stds_img = r_stds_img.repeat(64, 1)
        plt.imshow(r_stds_img.T)
        plt.colorbar(orientation='horizontal')
        plt.savefig(os.path.join(self.save_dir, '{}_route-err.jpg'.format(name)), bbox_inches='tight', dpi=300)
        plt.close()

        cnts = self.eval_stats['route-carrier-hist']

        if name not in self.__displayed_once:
            cnts_img = cnts.repeat(64, 1).repeat(K, 0)
            plt.imshow(cnts_img.T)
            plt.colorbar(orientation='horizontal')
            plt.savefig(os.path.join(self.save_dir, '{}_route-carrier-hist.jpg'.format(name)), bbox_inches='tight',
                        dpi=300)
            plt.close()

            np.savez(os.path.join(self.save_dir, '{}_route-carrier-err.npz'.format(name)),
                     std=r_stds, rc_std=rc_stds, cnt=cnts)

        err = np.array(list(self.err_stats.values()))
        np.savez(os.path.join(self.save_dir, '{}_route-carrier-pred-vs-real.npz'.format(name)), err)

        losses["avg_route"] = np.mean(r_stds)
        losses["avg_route_carrier"] = np.mean(rc_stds[cnts > 0])

        if self.opt.loss_weight_method == "weight":
            losses["avg_route"] *= self.opt.loss_weight
            losses["avg_route_carrier"] *= self.opt.loss_weight

        self.__displayed_once.add(name)
        return losses

    def __init_4_optimization(self):
        self.__load_freq_mask()
        self.max_influence = 0
        self.visual_ops = ['freq_mask', 'freq_optim']
        if self.opt.baseline is None:
            self.visual_ops += ['freq_optim_all']
        if self.opt.limit_max_freq:
            self.visual_ops += ['freq_max']
        self.__init_frequency()
        self.__load_cost_demand()
        self.opt.batchSize = self.opt.n_routes
        self.loss_names.append('influence')
        self.loss_names.append('over_budget')
        if self.opt.total_budget <= 0:
            self.opt.total_budget = self.cost[self.opt.opt_airline] * tf.reduce_sum(self.freq_orig)
        else:
            self.opt.total_budget *= self.opt.cost_scale
        self.over_budget_penalty = self.opt.over_budget_penalty
        if self.opt.what == 'optimize' and self.opt.budget_penalty_beta0 is not None:
            self.over_budget_penalty = tf.Variable(self.opt.over_budget_penalty, trainable=False)

        self.__load_ticket_price()

    def __init_embeding_config(self):
        self.mask4weight = None
        self.embed_online = False
        if self.opt.use_graph_feat:
            assert hasattr(self, "graph_feat")
        if self.opt.use_autoencoder:
            assert hasattr(self, "encoder_feat")
        if not self.opt.use_graph_feat:
            self.graph_feat = None
            self.n_graph_feat = 0
        if not self.opt.use_autoencoder:
            self.encoder_feat = None
            self.n_encoder_feat = 0

    def __init_frequency(self):
        freq = np.load(self.opt.init_freq_file)['freq']
        freq = freq.transpose(2, 0, 1).astype(np.float32)
        self.init_freq_all = tf.constant(freq)
        self.visual_ops += ['freq_orig', 'freq_orig_all']
        self.freq_orig_all = self.init_freq_all[self.opt.opt_airline]
        self.freq_orig_all_np = freq[self.opt.opt_airline]

        self.freq_max_np = np.max(freq, axis=0) * self.opt.max_freq_scale  # numpy freq max for group greedy python loop
        self.freq_max_np[self.freq_max_np == 0] = 100
        self.freq_max = tf.constant(self.freq_max_np)

        if self.freq_mask is None:
            self.freq_mask = tf.where(self.freq_orig_all > 0,
                                      tf.ones_like(self.freq_orig_all), tf.zeros_like(self.freq_orig_all))

            self.route_coords_np = np.array(np.where(freq[self.opt.opt_airline] > 0)).T  # N x 2 matrix

        self.freq_max_sparse_np = self.freq_max_np[self.route_coords_np[:, 0], self.route_coords_np[:, 1]]

        self.freq_orig = self.freq_orig_all * self.freq_mask

        if self.opt.baseline is not None:
            self.route_coords = tf.where(tf.greater(self.freq_mask, 0))

        with tf.variable_scope("frequency"):
            if self.opt.init_freq:
                self.best_freq_network = self.freq_orig_all_np.copy()
                self.freq_var = tf.Variable(self.freq_orig, name="freq")
            else:
                if self.opt.baseline is not None:
                    inifreq = tf.zeros((self.opt.n_airports, self.opt.n_airports))
                else:
                    inifreq = tf.random_uniform((self.opt.n_airports, self.opt.n_airports), 0, 0.001, )
                self.best_freq_network = np.zeros_like(self.freq_orig_all_np)
                self.freq_var = tf.Variable(inifreq * self.freq_mask, name="freq")

    def __init_baseline(self):
        self.best_freq_network_delay = None  # the best network for future update
        self.route_coords_np_it = iter(self.route_coords_np)
        if self.opt.baseline == "group_greedy":
            N = self.opt.n_routes // self.opt.ggr_n_group
            self.ggr_group_ns = np.ones(self.opt.ggr_n_group) * N
            self.ggr_group_ns[:self.opt.n_routes % self.opt.ggr_n_group] += 1

            self.group_route_ids, self.ggr_optim = [], []
            s = 0
            for n in self.ggr_group_ns:
                self.group_route_ids.append(np.arange(s, s + n))
                self.ggr_optim.append([[] for i in range(n)])
                s += n

            self.ggr_update_grp_idx = np.arange(self.opt.ggr_n_group)

        elif self.opt.baseline == "brutal":
            max_freq = np.max(self.freq_max_sparse_np)
            freqs = np.arange(0, max_freq, self.opt.gr_step)
            self.brutal_combs_it = itertools.product(freqs, repeat=self.route_coords_np.shape[0])

    def __load_cost_demand(self):
        data = np.load(self.opt.cost_demand)
        self.cost_np = data['cost'].astype(np.float32).flatten() * self.opt.cost_scale
        demand = data['demand'].astype(np.float32).flatten() * self.opt.demand_scale
        self.cost = tf.constant(self.cost_np)
        self.demand = tf.constant(demand)

    @staticmethod
    def __gather_feat(coords, feats_all, batchsize, n_carriers=8):
        # gather graph feature based on route id
        bidx = tf.reshape(tf.range(batchsize, dtype=tf.int64), (-1, 1))
        bidx = tf.tile(bidx, (1, 2))
        indices = tf.concat((tf.reshape(bidx, (-1, 1)), tf.reshape(coords, (-1, 1))), axis=1)
        feat = tf.gather_nd(feats_all, indices)
        feat = tf.reshape(feat, (batchsize, 2, n_carriers, -1))
        feat = tf.transpose(feat, (0, 2, 3, 1))
        feat = tf.reshape(feat, (batchsize, n_carriers, -1))
        return feat

    def __cal_all_features(self, inputs, freqi=None):
        feats = inputs[0]  # B x A x F
        # prepare the features
        if not self.embed_online and (self.opt.use_autoencoder or self.opt.use_graph_feat):
            # use precomputed features
            feat_concat = ConcatFeatLayer(self.opt.n_airlines, self.graph_feat, self.n_graph_feat, self.encoder_feat,
                                          self.n_encoder_feat)
            feat_net_ae = feat_concat((inputs[3], inputs[4]))
            feats = tf.concat((feats, feat_net_ae), -1)
            self.opt.n_features = feats.shape[-1]
            # load the pre-trained autoencoder but idle
            self.__cal_autoencoder_feat(feats, tf.zeros((1, self.opt.n_airports, self.opt.n_airports)))
        else:
            if self.opt.what == "optimize":
                if freqi is None:
                    # remove new routes and negative freqs, fix freqs that do not need optimization
                    freqi = tf.nn.relu(self.freq_mask * self.freq_var)

                if self.opt.limit_max_freq:
                    freqi = tf.clip_by_value(freqi, 0, self.freq_max)

                self.freq_optim = freqi
                freqi = self.freq_optim + (1 - self.freq_mask) * self.freq_orig_all
                self.freq_optim_all = freqi

                freq = tf.concat((self.init_freq_all[:self.opt.opt_airline], tf.expand_dims(freqi, 0),
                                  self.init_freq_all[self.opt.opt_airline + 1:]), 0)

                batch_freq = tf.gather_nd(tf.transpose(freq, (1, 2, 0)), inputs[4])  # B x A

                feats = tf.concat((tf.expand_dims(batch_freq, -1), feats[:, :, 1:]), -1)
            else:
                freq = tf.reshape(inputs[3], (-1, self.opt.n_airports, self.opt.n_airports))

            # add network feature
            if self.opt.use_graph_feat:
                feats = self.__cal_graph_feat(feats, freq, inputs[4])

            # add autoencoder
            if self.opt.use_autoencoder:
                feats = self.__cal_autoencoder_feat(feats, freq)

        return feats

    def __cal_graph_feat(self, feats, freq, coords):
        batchSize = self.opt.batchSize
        if self.opt.what == "optimize":
            batchSize = 1
        self.net_feat = GraphFeatLayer(self.opt.n_airports, batchSize * self.opt.n_airlines)
        feat_net = self.net_feat(freq)  # BA x N x 4
        n_feat = feat_net.shape[-1]
        feat_net = tf.reshape(feat_net, (-1, self.opt.n_airlines, self.opt.n_airports, n_feat))
        feat_net = tf.transpose(feat_net, (0, 2, 1, 3))  # B x A x N x 4 -> B x N x A x 4
        if self.opt.what == "optimize":
            feat_net = tf.tile(feat_net, (self.opt.batchSize, 1, 1, 1))

        feat_net = self.__gather_feat(coords, feat_net, self.opt.batchSize, self.opt.n_airlines)
        feats = tf.concat((feats, feat_net), -1)
        self.opt.n_features += n_feat * 2
        if self.opt.what == "train":
            tf.stop_gradient(feats)

        return feats

    def __cal_autoencoder_feat(self, feats, freq):
        if self.opt.max_Freq > 0:
            freq = freq / self.opt.max_Freq
        self.net_ae = EgoAutoencoderModel()
        self.net_ae.initialization(self.opt)
        feat_ae = self.net_ae.custom_encoder(freq, False)  # BA x k
        if self.embed_online:
            feat_ae = tf.reshape(feat_ae, (-1, self.opt.n_airlines, self.opt.encoder_hidden[-1]))

            if self.opt.what == "optimize":
                feat_ae = tf.tile(feat_ae, (self.opt.batchSize, 1, 1))

            feats = tf.concat((feats, feat_ae), -1)
            self.opt.n_features += self.opt.encoder_hidden[-1]
            # no need for gradient during training
            if self.opt.what == "train":
                feats = tf.stop_gradient(feats)

        return feats

    def __predict(self, inputs, train_pahse=True, **kwargs):
        if self.opt.net_reg == 'reg_mlp':
            pred = self.__reg_mlp(inputs, train_pahse, **kwargs)
        elif self.opt.net_reg == 'reg_mlp_combine':
            pred = self.__reg_mlp_combine(inputs, train_pahse, **kwargs)
        return pred

    # load autoencoder weights
    def __load_encoder(self, sess, **kwargs):
        varlist = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='encoder')
        loader = tf.train.Saver(varlist)
        loader.restore(sess, self.opt.pretrained_weights)

    # load autoencoder weights
    def __load_all_models(self, sess, **kwargs):
        varlist = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
        exclist = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='frequency')
        for exc in exclist:
            varlist.remove(exc)
        varlist_map = self.get_load_varlist(varlist)
        loader = tf.train.Saver(varlist_map)
        if self.opt.pretrained_weights is None:
            self.opt.pretrained_weights = self.save_dir + "/latest.ckpt"
        try:
            loader.restore(sess, self.opt.pretrained_weights)
        except ValueError:
            pass

    # mlp: each route a seperate model
    def __reg_mlp(self, input, train_phase, **kwargs):
        dropout = None
        self.input = input
        if "dropout" in kwargs.keys():
            dropout = kwargs['dropout']
        if len(self.opt.fc_nfeats) > 0:
            input_fc = tf.reshape(input[0], (-1, self.opt.n_airlines * self.opt.n_features))

            for fcn in self.opt.fc_nfeats:
                fc_layer = JamimFCLayer(self.opt.n_routes, self.opt.n_airlines, fcn)
                input_fc = fc_layer((input_fc, input[-1]))
                if dropout is not None:
                    input_fc = tf.layers.dropout(input_fc, dropout, training=train_phase)

            input_fc = tf.reshape(input_fc, (-1, self.opt.n_airlines, self.opt.fc_nfeats[-1]))
            input = (input_fc, input[-1])

        multilogit = JamimMultilogitLayer(self.opt.n_routes, self.opt.n_airlines, uniform_max=self.opt.jamim_ini_max,
                                          kernel_initializer=self.opt.init_type)

        pred = multilogit(input)
        return pred

    # mlp: all routes using the same model
    def __reg_mlp_combine(self, input, train_phase, **kwargs):
        input_fc = tf.reshape(input[0], tf.constant((-1, self.opt.n_airlines * self.opt.n_features)))

        if len(self.opt.fc_nfeats) > 0:
            for fcn in self.opt.fc_nfeats:
                input_fc = tf.layers.dense(input_fc, fcn)

        input_fc = tf.layers.dense(input_fc, self.opt.n_airlines)
        pred = tf.nn.softmax(input_fc)
        return pred

    def __costum_loss(self, real, pred, **kwargs):
        if self.opt.what == "optimize":
            self.__custom_optimize_loss(real, pred, **kwargs)
        else:
            self.__custom_train_loss(real, pred, **kwargs)

    def __custom_train_loss(self, real, pred, **kwargs):
        if self.opt.loss_weight_method == "weight" and self.opt.loss_weight > 0:
            weights = tf.cast(real > 0, tf.float32) * self.opt.loss_weight
        elif self.opt.loss_weight_method == "freq":
            if self.freq4weight is not None:
                route = tf.reshape(kwargs['route'], (-1, 1))
                weights = tf.gather_nd(self.freq4weight, route)
            else:
                weights = kwargs['freq']
                if self.opt.max_Freq > 0:
                    weights = weights / self.opt.max_Freq
        else:
            if self.mask4weight is not None:
                route = tf.reshape(kwargs['route'], (-1, 1))
                weights = tf.gather_nd(self.mask4weight, route)
            else:
                weights = tf.cast(real > 0, tf.float32)

        self.loss_cost = tf.losses.mean_squared_error(real, pred, weights)

    def __custom_optimize_loss(self, real, pred, **kwargs):
        passenger = pred[:, self.opt.opt_airline] * self.demand
        cost = tf.reduce_sum(self.cost[self.opt.opt_airline] * self.freq_optim)
        self.loss_influence = tf.reduce_sum(passenger)
        self.loss_over_budget = (cost - self.opt.total_budget)
        if self.ticket_price is not None:
            self.loss_profit = tf.reduce_sum(self.loss_influence * passenger) - cost

        if self.opt.budget_normalize:
            self.loss_over_budget = self.loss_over_budget / self.opt.total_budget

        if self.opt.budget_penalty_beta0 is not None:
            updates = self.__adapt_budget_penalty(self.opt.jamim_lr_decay is not None)
            try:
                tfprint = tf.print("influence: ", self.loss_influence, "over budget: ", self.loss_over_budget,
                                   "penalty:", updates[0], 'learning rate: ', updates[1])
                updates += [tfprint]
            except:
                pass
            with tf.control_dependencies(updates):
                loss_over_budget = self.__get_flight_cost_penalty()
        else:
            loss_over_budget = self.__get_flight_cost_penalty()

        if self.ticket_price is not None:
            self.loss_cost = -self.loss_profit + loss_over_budget
        else:
            self.loss_cost = -self.loss_influence + loss_over_budget

    def __get_flight_cost_penalty(self):
        if self.opt.optim_method == "lagrange":
            loss_over_budget = self.loss_over_budget ** 2 * self.over_budget_penalty / 2
        else:
            loss_over_budget = tf.nn.relu(self.loss_over_budget) * self.over_budget_penalty
        return loss_over_budget

    def __adapt_budget_penalty(self, update_learning_rate=True):
        def fix_penalty():
            learning_rate = 0.0
            if update_learning_rate:
                learning_rate = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, "learning_rate")[0]
            return self.opt.over_budget_penalty, learning_rate

        def adapt_penalty():
            if self.ticket_price is not None:
                df = tf.gradients(self.loss_profit, self.freq_var)[0]
            else:
                df = tf.gradients(self.loss_influence, self.freq_var)[0]
            dg = tf.gradients(self.loss_over_budget, self.freq_var)[0]
            sum_dg_dg = tf.reduce_sum(dg * dg)

            def reach_max_freq():
                return 0.0, 0.0

            def not_reach_max_freq():
                pen = self.loss_over_budget * self.opt.budget_penalty_beta0 + tf.reduce_sum(df * dg) / sum_dg_dg
                if self.opt.optim_method == "lagrange":
                    pen = pen / self.loss_over_budget
                learning_rate = 0.0
                if update_learning_rate:
                    learning_rate = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, "learning_rate")[0]
                    if self.opt.jamim_lr_decay == 0.0:
                        learning_rate = self.opt.lr * self.loss_over_budget
                    else:
                        learning_rate = learning_rate * self.opt.jamim_lr_decay
                return pen, learning_rate

            return tf.cond(tf.equal(sum_dg_dg, 0), reach_max_freq, not_reach_max_freq)

        pen, lr = tf.cond(self.loss_over_budget > 0, adapt_penalty, fix_penalty)

        update_penalty = self.over_budget_penalty.assign(pen)
        updates = [update_penalty]
        if update_learning_rate:
            learning_rate = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, "learning_rate")[0]
            update_lr = learning_rate.assign(lr)
            updates += [update_lr]

        return updates

    def __load_freq_mask(self):
        self.freq4weight, self.mask4weight, self.mask4weight_mat = None, None, None
        if self.opt.freq_mask_file is not None:
            data = np.load(self.opt.freq_mask_file)
            self.freq4weight = tf.constant(data['freq'] / np.max(data['freq']), dtype=tf.float32)
            self.mask4weight = tf.constant(data['mask'] > 0, dtype=tf.float32)

            mask4weight_mat = np.zeros((self.opt.n_airports, self.opt.n_airports), np.float32)
            # mask per airline because airline with zero freq was not trained to predict with nonzero freq
            mask4weight_mat[list(data['coords'].T)] = data['mask'][:, self.opt.opt_airline] > 0

            self.route_coords_np = np.array(np.where(mask4weight_mat)).T
            self.route_coords = tf.constant(self.route_coords_np, dtype=tf.int64)

            self.freq_mask = tf.constant(mask4weight_mat)

    def __load_ticket_price(self):
        self.ticket_price = None
        if self.opt.ticket_price_file is not None:
            self.loss_names[1] = "profit"
            self.loss_names.append('influence')

            price = np.load(self.opt.ticket_price_file)
            price = tf.constant(price[:, :, self.opt.opt_airline])
            self.ticket_price = tf.gather_nd(price, self.route_coords)

    def __baseline_one_step_loop_gr(self, inputs):
        def gr_cond_inner(i, max_influence, pred, old_freq, new_freq):
            return tf.greater(tf.shape(self.route_coords)[0], i)

        def gr_body_inner(i, max_influence, pred, old_freq, new_freq):
            delta = tf.SparseTensor(indices=[self.route_coords[i]], values=[self.opt.gr_step],
                                    dense_shape=[self.opt.n_airports, self.opt.n_airports])
            freqi = tf.sparse_add(old_freq, delta)
            feats = self.__cal_all_features(inputs, freqi)
            inputsi = (feats, inputs[2])

            with tf.variable_scope('regression') as scope:
                predi = self.__predict(inputsi, False)
                scope.reuse_variables()

            influence = tf.reduce_sum(predi[:, self.opt.opt_airline] * self.demand)

            def update():
                max_influence, new_freq, pred = influence, freqi, predi
                return max_influence, new_freq, pred

            def void():
                return max_influence, new_freq, pred

            max_influence, new_freq, pred = tf.cond(influence > max_influence, update, void)
            return i + 1, max_influence, pred, old_freq, new_freq

        idx = tf.constant(0)
        max_influence_i = tf.constant(0.0)
        pred_i = tf.zeros_like(inputs[1])
        new_freq_i = tf.zeros((self.opt.n_airports, self.opt.n_airports))
        old_freq_i = self.freq_var * self.freq_mask

        _, max_influence, pred, old_freq, freq_optim = tf.while_loop(gr_cond_inner, gr_body_inner,
                                                                     (idx, max_influence_i, pred_i, old_freq_i,
                                                                      new_freq_i))

        self.freq_optim = self.freq_var.assign(freq_optim)
        return pred

    def __baseline_greedy_update_before_predict(self, sess):
        try:
            coord = next(self.route_coords_np_it)
            freqi = self.best_freq_network.copy()
            freqi[coord[0], coord[1]] += self.opt.gr_step

            update_freq = tf.assign(self.freq_var, freqi)
            sess.run(update_freq)
        except StopIteration:
            self.best_freq_network = self.best_freq_network_delay  # update the best network
            self.route_coords_np_it = iter(self.route_coords_np)  # reset the iterator
            self.__baseline_greedy_update_before_predict(sess)
        return True

    def __baseline_brutal_update_before_predict(self, sess):
        while True:
            try:
                freq = next(self.brutal_combs_it)
                cost = np.sum(np.array(freq) * self.cost_np[self.opt.opt_airline])
                if np.sum(freq <= self.freq_max_sparse_np) == self.route_coords_np.shape[0] and cost < self.opt.total_budget:
                    freqi = tf.sparse_tensor_to_dense(tf.SparseTensor(
                        self.route_coords_np, freq, (self.opt.n_airports, self.opt.n_airports)))
                    update_freq = tf.assign(self.freq_var, freqi)
                    sess.run(update_freq)
                    return True
            except StopIteration:
                return False

    def __baseline_group_greedy_update_before_predict(self, sess):
        # dynamic programming main loop, over routes in this group
        pass