import tensorflow as tf

def define_encoder_4test(inputs):
    # encoder
    # 32 x 32 x 1   ->  16 x 16 x 32
    # 16 x 16 x 32  ->  8 x 8 x 16
    # 8 x 8 x 16    ->  2 x 2 x 8
    net = tf.layers.conv2d(inputs, 32, [5, 5], strides=(2, 2), padding='SAME', activation='relu')
    net = tf.layers.conv2d(net, 16, [5, 5], strides=(2, 2), padding='SAME', activation='relu')
    net = tf.layers.conv2d(net, 8, [5, 5], strides=(4, 4), padding='SAME', activation='relu')
    return net

def define_decoder_4test(inputs):
    # decoder
    # 2 x 2 x 8    ->  8 x 8 x 16
    # 8 x 8 x 16   ->  16 x 16 x 32
    # 16 x 16 x 32  ->  32 x 32 x 1
    net = tf.layers.conv2d_transpose(inputs, 16, [5, 5], strides=(4, 4), padding='SAME', activation='relu')
    net = tf.layers.conv2d_transpose(net, 32, [5, 5], strides=(2, 2), padding='SAME', activation='relu')
    net = tf.layers.conv2d_transpose(net, 1, [5, 5], strides=(2, 2), padding='SAME', activation=tf.nn.tanh)
    return net