import tensorflow as tf


class GraphFeatLayer(tf.keras.layers.Layer):
    def __init__(self, nrow, batchsize=64, ncol=None):
        super(GraphFeatLayer, self).__init__()
        self.surfer_prob = 0.15
        self.nrow = nrow
        self.ncol = ncol
        if self.ncol is None:
            self.ncol = self.nrow
        self.batchsize = batchsize

    def call(self, inputs, threshold=1.0e-6, *args, **kwargs):
        net_feats = []
        for i in range(self.batchsize):
            net_feats += [self._proc_one(inputs[i], threshold)]
        outputs = tf.stack(net_feats)
        return outputs

    def _proc_one(self, A, threshold):
        out_deg = tf.reduce_sum(A, axis=1, keepdims=True)  # sum along rows
        in_deg = tf.reduce_sum(A, axis=0, keepdims=True)  # sum along column
        # calculate ego_density: weight_sum/n(n+1), n: number of neighbours
        A_sum = tf.add(A, tf.transpose(A))
        n_vect = tf.count_nonzero(A_sum, 1, keepdims=True)
        n_vect = tf.cast(tf.map_fn(lambda x: x * (x + 1), n_vect), tf.float32)
        edge_wgs_sum = tf.reduce_sum(A_sum, axis=1, keepdims=True)
        density = tf.div_no_nan(edge_wgs_sum, n_vect)

        X2 = tf.multiply(tf.ones([self.nrow, self.ncol], tf.float32),
                         (self.surfer_prob / tf.cast(self.nrow, tf.float32)))
        A_norm = tf.div_no_nan(A, in_deg)  # normalize A
        B = tf.add(tf.multiply((1 - self.surfer_prob), A_norm), X2)

        v = tf.ones((self.nrow, 1))

        def cond(v, B, delta, threshold):
            return tf.greater(delta, threshold)

        def body(v, B, delta, threshold):
            v1 = tf.matmul(B, v)
            v1 = v1 / tf.norm(v1, ord=1)
            delta = tf.reduce_sum(tf.square(tf.subtract(v1, v)))
            v = v1
            return v, B, delta, threshold

        delta = tf.constant(float('inf'))
        v, _, _, _ = tf.while_loop(cond, body, (v, B, delta, threshold))

        # hub = tf.div(tf.ones((1, self.nrow)), self.nrow)
        # authority = hub
        #
        # def cond_hits(A, hub, authority, delta, threshold):
        #     return tf.greater(delta, threshold)
        #
        # def body_hits(A, hub, authority, delta, threhold):
        #     hub1 = tf.matmul(authority, tf.transpose(A))
        #     authority1 = tf.matmul(hub1, A)
        #     #hub_sum = tf.reduce_sum(tf.square(hub1))
        #     hub_sum = tf.reduce_sum(hub1)
        #     hub1 = tf.div_no_nan(hub1, hub_sum)
        #     #authority_sum = tf.reduce_sum(tf.square(authority1))
        #     authority_sum = tf.reduce_sum(authority1)
        #     authority1 = tf.div_no_nan(authority1, authority_sum)
        #
        #     delta1 = tf.reduce_sum(tf.square(tf.subtract(hub1, hub)))
        #     delta2 = tf.reduce_sum(tf.square(tf.subtract(authority1, authority)))
        #
        #     delta = tf.maximum(delta1, delta2)
        #     hub, authority = hub1, authority1
        #     return A, hub, authority, delta, threhold
        #
        # delta = tf.constant(float('inf'))
        # _, hub, authority, _, _ = tf.while_loop(cond_hits, body_hits, (A_norm, hub, authority, delta, threshold))
        #
        # net_feat = tf.stack([tf.squeeze(hub), tf.squeeze(authority), tf.squeeze(v),
        #                      tf.squeeze(in_deg), tf.squeeze(out_deg), tf.squeeze(density)], axis=1)

        net_feat = tf.stack([tf.squeeze(v), tf.squeeze(in_deg), tf.squeeze(out_deg), tf.squeeze(density)], axis=1)
        return net_feat


if __name__ == "__main__":
    import numpy as np
    N = 60
    data = np.random.rand(2, N, N).astype(np.float32)
    # data = np.zeros((2, N, N), dtype=np.float32)
    ''''data = np.array([[[0, 0, .3333, 0, .333, 0, .25],
                      [0, .5, .3333, 0, 0, .5, 0],
                      [1, 0, .3333, .3333, 0, 0, 0],
                      [0, 0, 0, .3333, .333, 0, 0],
                      [0, 0, 0, 0, 0, 0, .25],
                      [0, 0, 0, 0, 0, .5, .25],
                      [0, .5, 0, .3333, .333, 0, .25]],
                     [[0, 0, .3333, 0, .333, 0, .25],
                      [0, .5, .3333, 0, 0, .5, 0],
                      [1, 0, .3333, .3333, 0, 0, 0],
                      [0, 0, 0, .3333, .333, 0, 0],
                      [0, 0, 0, 0, 0, 0, .25],
                      [0, 0, 0, 0, 0, .5, .25],
                      [0, .5, 0, .3333, .333, 0, .25]]
                     ]).astype(np.float32)'''''
    X = tf.Variable(tf.constant(data))
    X = 2*X
    model = GraphFeatLayer(N, 2)
    # model = GraphFeatLayer(7, 2, 7)
    y = model(X)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        res = sess.run(y)
        print(res.shape)
        print(res[:, 0])

    # test with networkx for pagerank calculation
    import networkx as nx

    G = nx.from_numpy_matrix(2*data[0].T, create_using=nx.DiGraph)
    pr = nx.pagerank(G)
    #print(pr)

    hits = nx.hits(G)
    print(hits[0].values())
