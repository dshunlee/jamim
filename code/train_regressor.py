import tensorflow as tf
from options.train_options import TrainOptions
from data.airline_reg_data import AirlineRegData
from models.jamim_regression_model import JamimRegressionModel
from models.graph_feature_layer import GraphFeatLayer
from models.ego_autoencoder_model import EgoAutoencoderModel
import models.train_utils as train_utils
import models.baseline_utils as baseline_utils
from util.visualizer import Visualizer
import multiprocessing
import numpy as np
import os

import sys
sys.path.append('../../../')


def train(opt):
    # if optmization, load all of the routes
    if opt.what == "optimize":
        opt.batchSize = opt.n_routes
        opt.isTrain = False
        # opt.display_freq = 1
        # opt.save_epoch_freq = 1
        # opt.print_freq = 1

    graph_feat, encoder_feat, load_frequency = None, None, True
    if opt.freq_files is not None and opt.what != "optimize":
        graph_feat, encoder_feat = pre_process(opt)
        load_frequency = False

    # prepare data
    train_dataset, val_dataset, test_dataset, train_dataset_one_epoch = prepare_data(opt, load_frequency)

    # initialize model
    jamim = JamimRegressionModel(graph_feat=graph_feat, encoder_feat=encoder_feat)

    if opt.what == "evaluate":
        train_utils.eval(opt, jamim, train_dataset_one_epoch, val_dataset, test_dataset)
    else:
        # create visualizer
        visualizer = Visualizer(opt)

        if opt.baseline is not None:
            baseline_utils.train(opt, jamim, train_dataset, visualizer)
        else:
            train_utils.train(opt, jamim, train_dataset, val_dataset, test_dataset, train_dataset_one_epoch, visualizer)

def pre_process(opt):
    def get_files(opt):
        filelist = []
        base_dir = os.path.dirname(opt.freq_files)
        with open(opt.freq_files) as fin:
            str = fin.readline().strip()
            while str:
                filelist.append(base_dir + "/" + str)
                str = fin.readline().strip()
        return filelist

    def pre_proc_features(files, opt):
        freq_holder = tf.placeholder(tf.float32, (opt.n_airlines, opt.n_airports, opt.n_airports))
        feats = []
        proc_graphfeat, proc_codes = {}, {}

        net_feat = GraphFeatLayer(opt.n_airports, opt.n_airlines)
        feat_net = net_feat(freq_holder)
        feat_net = tf.transpose(feat_net, (1, 0, 2))
        feats += [feat_net]
        tf.stop_gradient(feat_net)

        freq = freq_holder
        if opt.max_Freq > 0:
            freq = freq / opt.max_Freq

        net_ae = EgoAutoencoderModel()
        net_ae.initialization(opt)
        feat_ae = net_ae.custom_encoder(freq, False)
        tf.stop_gradient(feat_ae)
        feats += [feat_ae]

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        # start preprocessing
        with tf.Session(config=config) as sess:
            sess.run(tf.global_variables_initializer())
            if opt.use_autoencoder:
                varlist = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='encoder')
                loader = tf.train.Saver(varlist)
                loader.restore(sess, opt.pretrained_weights)

            for fi in files:
                freq_data = np.load(fi)['freq'].transpose(2, 0, 1)
                freq_data = freq_data.astype(np.float32)
                codes = sess.run(feats, feed_dict={freq_holder: freq_data})
                proc_graphfeat[fi] = codes[0]
                proc_codes[fi] = codes[1]
            sess.close()
            return proc_graphfeat, proc_codes

    def pre_proc(opt, return_dict):
        files = get_files(opt)
        graph_feat, encoder_feat = pre_proc_features(files, opt)
        return_dict['graph'] = graph_feat
        return_dict['encoder'] = encoder_feat

    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    p = multiprocessing.Process(target=pre_proc, args=(opt, return_dict))
    p.start()
    p.join()

    return return_dict['graph'], return_dict['encoder']


def prepare_data(opt, load_frequency=True):
    if opt.what == 'optimize':
        train_data_loader = AirlineRegData(opt.dataroot, opt.n_features, opt.n_airlines, opt.n_airports,
                                           load_frequency, opt.normarlize_file, opt.optim_route_idx)
    else:
        train_data_loader = AirlineRegData(opt.dataroot, opt.n_features, opt.n_airlines, opt.n_airports,
                                           load_frequency, opt.normarlize_file)
    train_dataset = train_data_loader.prepare_data()
    if opt.what == "evaluate":
        train_dataset_one_epoch = train_dataset.batch(12)
    else:
        train_dataset_one_epoch = train_dataset.batch(opt.batchSize)

    if opt.what == "train":
        train_dataset = train_dataset.shuffle(buffer_size=2048)

    train_dataset = train_dataset.repeat().batch(opt.batchSize)

    if opt.what == "optimize":
        val_dataset = None
        test_dataset = None
    else:
        val_data_loader = AirlineRegData(opt.dataroot.replace('train', 'val'),
                                         opt.n_features, opt.n_airlines, opt.n_airports,
                                         load_frequency, opt.normarlize_file)
        val_dataset = val_data_loader.prepare_data()
        val_dataset = val_dataset.batch(opt.n_routes, drop_remainder=False)

        test_data_loader = AirlineRegData(opt.dataroot.replace('train', 'test'),
                                          opt.n_features, opt.n_airlines, opt.n_airports,
                                          load_frequency, opt.normarlize_file)
        test_dataset = test_data_loader.prepare_data()
        test_dataset = test_dataset.batch(opt.n_routes, drop_remainder=False)

    return train_dataset, val_dataset, test_dataset, train_dataset_one_epoch


if __name__ == '__main__':
    opt = TrainOptions().parse()
    train(opt)