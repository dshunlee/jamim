import os, sys
import scipy.sparse as sp
import argparse
import numpy as np
from data.airline_reg_data import AirlineRegData
from data.data_util import _float_feature, _int64_feature, _bytes_feature
from models.graph_feature_layer import GraphFeatLayer
import tensorflow as tf
import matplotlib.pyplot as plt


def convert(args):
    if not os.path.isfile(args.input):
        raise IOError('Not a file: {}'.format(args.input))
    filelist = []
    base_dir = os.path.dirname(args.input)
    with open(args.input) as fin:
        str = fin.readline().strip()
        while str:
            filelist.append(base_dir + '/' + str)
            str = fin.readline().strip()

    return airline_to_tfrecords(filelist, args.adj_mat, args.output_file, args.normalize_file, args.cost_demand)


def airline_to_tfrecords(input_files, adj_file, tfrecord_file, normalize_file=None, cost_demand_file=None):
    writer = tf.python_io.TFRecordWriter(tfrecord_file)
    # load routes file
    adj = sp.load_npz(adj_file)
    adj = adj.tocoo()
    coords = np.vstack((adj.row, adj.col)).T

    # load cost demand
    if cost_demand_file is not None:
        cost_demand = np.load(cost_demand_file)
        demand = cost_demand['demand'][[adj.row, adj.col]]
        np.savez(tfrecord_file.replace('train.tfrecords', 'cost_demand.npz'),
                 cost=cost_demand['cost'], demand=demand)

    # load feature file
    data = np.load(input_files[0])

    NF, NAL, NAP = 0, 0, 0
    #classic = ['freq', 'avg_delay', 'delay_ratio', 'cancel_ratio', 'divert_ratio', 't_price']
    classic = list(data.keys())
    classic.remove('mrktshare')
    classic.remove('freq')
    classic.insert(0, 'freq')
    print("classic features: {}".format(classic))
    netfeat = ['page_rank', 'in_degree', 'out_degree', 'ego_density']
    NAL = data['mrktshare'].shape[-1]
    NAP = data['mrktshare'].shape[0]

    index = np.array(adj.row) * NAP + np.array(adj.col)
    NR = index.shape[0]
    print("The dataset has {} routes!".format(index.shape[0]))

    def organize_feats(data, feats):
        x = []
        for key in feats:
            if key in data.keys():
                if data[key].ndim == 2:
                    feati_0 = data[key][adj.row]
                    feati_1 = data[key][adj.col]
                    feati = np.stack((feati_0, feati_1)).transpose(1, 2, 0)
                elif data[key].ndim == 3:
                    feati = data[key].reshape(NAP * NAP, -1)
                    feati = feati[index]
                    feati = np.expand_dims(feati, -1)
                x.append(feati)
        x = np.concatenate(x, axis=-1)
        return x

    exmp = organize_feats(data, classic)
    NF = exmp.shape[-1]

    max_min = np.zeros((2, NAL, NF))
    max_min[1] = np.inf
    freq_cnt = np.zeros((NR, NAL))
    freq_mask = np.zeros((NR, NAL))
    for fi in range(len(input_files)):
        # print how many matrix are saved every 1000 images
        if not fi % 1000:
            print('Train data: {}/{}'.format(fi, len(input_files)))
            sys.stdout.flush()

        data = np.load(input_files[fi])
        feat = organize_feats(data, classic)
        label = data['mrktshare'].reshape(NAP*NAP, -1)[index]

        freq_cnt += feat[:, :, 0]
        freq_mask += feat[:, :, 0] > 0

        max_min[0] = np.maximum(max_min[0], feat.max(axis=0))
        max_min[1] = np.minimum(max_min[1], feat.min(axis=0))

        r_id = np.arange(0, NR).astype(np.int64)

        valid_idx = (label.sum(axis=1) - 1 < 1e-3)
        assert np.sum(valid_idx) == r_id.shape[0]

        feat = feat[r_id, :].reshape(-1, NAL*feat.shape[-1])
        label = label[r_id, :]
        r_id = r_id[r_id]
        coordsi = coords[r_id]

        np.savez(tfrecord_file.replace('.tfrecords', '-data.npz'), feat=feat, label=label, r_id=r_id, coords=coordsi, freq=input_files)

        for ri in range(r_id.shape[0]):
            feature = {'train/features': _float_feature(feat[ri]),
                       'train/label': _float_feature(label[ri]),
                       'train/route': _int64_feature([ri]),
                       'train/coords': _int64_feature(coordsi[ri]),
                       'train/freq': _bytes_feature(tf.compat.as_bytes(input_files[fi]))}

            example = tf.train.Example(features=tf.train.Features(feature=feature))
            # Serialize to string and write on the file
            writer.write(example.SerializeToString())

    writer.close()
    sys.stdout.flush()

    plt.imshow(freq_cnt.repeat(64, 1).T)
    plt.colorbar(orientation='horizontal')
    plt.savefig(tfrecord_file.replace('.tfrecords', '-freq.jpg'), bbox_inches='tight', dpi=300)
    plt.close()

    plt.imshow(freq_mask.repeat(64, 1).T)
    plt.colorbar(orientation='horizontal')
    plt.savefig(tfrecord_file.replace('.tfrecords', '-mask.jpg'), bbox_inches='tight', dpi=300)
    plt.close()

    np.savez(tfrecord_file.replace('.tfrecords', '-freq.npz'), freq=freq_cnt, mask=freq_mask, coords=coords)

    normalize = np.zeros((2, NAL, NF))
    normalize[0] = np.mean(max_min, axis=0)
    normalize[1] = (max_min[0] - max_min[1]) / 2
    if normalize_file is not None:
        np.save(normalize_file, normalize)

    print('convert done!')
    return NR, NAL, NF


def check_data(args, NA, NF):
    with tf.Session() as sess:
        batchsize = 1
        ncarrier = 8
        jamim = AirlineRegData(args.output_file, n_features=NF, n_airlines=NA)
        dataset = jamim.prepare_data()
        dataset = dataset.batch(batchsize, drop_remainder=False)

        batchi = dataset.make_one_shot_iterator().get_next()

        netfeat_model = GraphFeatLayer(60, batchsize * ncarrier)

        freq = tf.reshape(batchi[3], (-1, 60, 60))
        feat_net = netfeat_model(freq)
        feat_net = tf.reshape(feat_net, (batchsize, ncarrier, 60, -1))
        feat_net = tf.transpose(feat_net, (0, 2, 1, 3))  # B x A x N x 4 -> B x N x A x 4
        feat_net = gather_feat(batchi[-1], feat_net, batchsize, ncarrier)

        sess.run(tf.global_variables_initializer())

        for i in range(2):
            xx = sess.run([batchi, freq, feat_net])
            # value = sess.run(dx, feed_dict={dx: xx})
            print(xx)
        sess.close()


def gather_feat(coords, feats_all, batchsize, n_carriers=8):
    bidx = tf.reshape(tf.range(batchsize, dtype=tf.int64), (-1, 1))
    bidx = tf.tile(bidx, (1, 2))
    indices = tf.concat((tf.reshape(bidx, (-1, 1)), tf.reshape(coords, (-1, 1))), axis=1)
    feat = tf.gather_nd(feats_all, indices)
    feat = tf.reshape(feat, (batchsize, 2, n_carriers, -1))
    feat = tf.transpose(feat, (0, 2, 3, 1))
    feat = tf.reshape(feat, (batchsize, n_carriers, -1))
    return feat


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='data preparing.')
    parser.add_argument('what', type=str, metavar='convert',
                        help='convert or test')
    parser.add_argument('--input', '-i', type=str, default='./data/features/train/filelist.txt', help='path to npz file list')
    #parser.add_argument('--autoencoder', '-c', type=str, default='data/train-codes.npy', help='path to autoencoder code file, npy format')
    parser.add_argument('--output_file', '-o', type=str, default='./data/features/a60f8_train.tfrecords', help='path to output tfrecords file')
    parser.add_argument('--normalize_file', '-n', type=str, default='./data/features/normalize.npy',
                        help='path to normalize file')
    parser.add_argument('--adj_mat', '-a', type=str, default='./data/r10.npz', help='path to adjacency matrix')
    parser.add_argument('--cost_demand', '-cd', type=str, default='./data/cost_demand.npz', help='path to cost demand data')

    args = parser.parse_args()

    if args.what == 'convert':
        NR, NAL, NF = convert(args)
    else:
        raise NotImplementedError('{} is not implemented!'.format(args.what))

    check_data(args, NAL, NF)