import numpy as np
import os
import time
from . import util
from . import html


class Visualizer():
    def __init__(self, opt):
        self.display_id = opt.display_id
        self.use_html = opt.isTrain and not opt.no_html
        self.win_size = opt.display_winsize
        self.name = opt.name
        self.opt = opt
        self.saved = False
        self.n_samples = 0
        self.legends = []

        if self.display_id > 0:
            import visdom
            self.ncols = opt.display_ncols
            self.vis = visdom.Visdom(server=opt.display_server, port=opt.display_port, raise_exceptions=True)
            self.vis.close()
        if self.use_html:
            self.web_dir = os.path.join(opt.checkpoints_dir, opt.name, 'web')
            self.img_dir = os.path.join(self.web_dir, 'images')
            print('create web directory {}...'.format(self.web_dir))
            util.mkdirs([self.web_dir, self.img_dir])

        self.log_name = os.path.join(opt.checkpoints_dir, opt.name, 'loss_log.txt')
        self.write_options(opt)
        with open(self.log_name, "a") as log_file:
            now = time.strftime("%c")
            log_file.write('================ Training Loss (%s) ================\n' % now)

    def write_options(self, opt):
        message = ''
        message += '\n\n================== Options ==================\n'
        for k, v in sorted(vars(opt).items()):
            comment = ''
            message += '{:>25}: {:<30}{}\n'.format(str(k), str(v), comment)
        message += '----------------- End -------------------'

        # save to the disk
        save_dir = os.path.join(opt.checkpoints_dir, opt.name)
        util.mkdirs(save_dir)
        with open(self.log_name, 'a') as opt_file:
            opt_file.write(message)
            opt_file.write('\n')

    def reset(self):
        self.saved = False

    # |visuals|: dictionary of images to display or save
    def display_current_results(self, visuals, epoch, save_result):
        # show images in the browser
        if self.display_id > 0:
            idx = 1
            for label, tensor in visuals.items():
                if isinstance(tensor, np.ndarray):
                    ndim = tensor.ndim

                if ndim == 2:
                    try:
                        self.vis.image(
                            tensor,
                            opts=dict(title=label),
                            win=self.display_id + idx)
                    except ConnectionError:
                        self.throw_visdom_connection_error()

                elif ndim == 3:
                    try:
                        self.vis.image(
                            tensor.transpose([2, 0, 1]),
                            opts=dict(title=label),
                            win=self.display_id + idx)
                    except ConnectionError:
                        self.throw_visdom_connection_error()

                idx += 1

        # save images, and show in an html file
        if self.use_html and (save_result or not self.saved):
            print('Saving to html...')
            self.saved = True
            for label, tensor in visuals.items():
                if isinstance(tensor, np.ndarray):
                    ndim = tensor.ndim

                if ndim == 3:
                    image_numpy = util.tensor2im(tensor)
                    img_path = os.path.join(self.img_dir, 'epoch{:03d}_{:s}.jpg'.format(epoch, label))
                    util.save_image(image_numpy, img_path)

            # update website
            webpage = html.HTML(self.web_dir, 'Experiment name = {:s}'.format(self.name), reflesh=1)
            for n in range(epoch, 0, -1):
                webpage.add_header('epoch [%d]' % n)

                ims, im_txts, im_links = [], [], []

                for label, tensor in visuals.items():
                    if isinstance(tensor, np.ndarray):
                        ndim = tensor.ndim

                    if ndim == 3:
                        im_path = os.path.join('images', 'epoch{:03d}_{:s}.jpg'.format(n, label))
                        ims.append(im_path)
                        im_txts.append(label)
                        im_links.append(im_path)

                if len(ims) > 0:
                    webpage.add_images(ims, im_txts, im_links, width=self.win_size)

            webpage.save()

    # losses: dictionary of error labels and values
    def plot_current_losses(self, epoch, counter_ratio, losses):
        x = np.array([epoch + counter_ratio])
        new_legend, old_legend = [], []
        for k in losses.keys():
            if k not in self.legends:
                self.legends.append(k)
                new_legend += [k]
            else:
                old_legend += [k]
        for i in range(len(new_legend)):
            try:
                update = None
                if self.vis.win_exists(str(self.display_id)):
                    update = "new"
                self.vis.line(
                    X=x,
                    Y=np.array([losses[new_legend[i]]]),
                    name=new_legend[i],
                    update=update,
                    win=str(self.display_id))
            except ConnectionError:
                self.throw_visdom_connection_error()

        for i in range(len(old_legend)):
            try:
                self.vis.line(
                    X=x,
                    Y=np.array([losses[old_legend[i]]]),
                    update='append',
                    name=old_legend[i],
                    win=str(self.display_id))
                self.vis.update_window_opts(
                    win=str(self.display_id),
                    opts={
                        'title': self.name + ' loss over time',
                        'legend': self.legends,
                        'xlabel': 'epoch',
                        'ylabel': 'loss'},
                )
            except ConnectionError:
                self.throw_visdom_connection_error()

        self.n_samples += 1

    def throw_visdom_connection_error(self):
        print('\n\nCould not connect to Visdom server (https://github.com/facebookresearch/visdom) for displaying training progress.\nYou can suppress connection to Visdom using the option --display_id -1. To install visdom, run \n$ pip install visdom\n, and start the server by \n$ python -m visdom.server.\n\n')
        exit(1)

    # losses: same format as |losses| of plot_current_losses
    def print_current_losses(self, epoch, i, losses, t_model, t_data):
        message = '(epoch: %d, iters: %d, model time: %.5f, data time: %.5f) ' % (epoch, i, t_model, t_data)
        for k, v in losses.items():
            message += '%s: %.5f ' % (k, v)

        print(message)
        with open(self.log_name, "a") as log_file:
            log_file.write('%s\n' % message)
