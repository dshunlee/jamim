import matplotlib.pyplot as plt
import scipy
import scipy.sparse as sp
from scipy import stats
import numpy as np

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def log(mat):
    mat = np.log10(mat)
    mat = mat + 1
    mat[np.isinf(mat)] = 0
    mat = mat/np.max(mat)
    return mat


def vis_adj_mat(file, nbin=50, map=log):
    mat = sp.load_npz(file)
    mat = mat.todense()
    x = np.array(scipy.stats.itemfreq(np.array(mat).flatten()))
    #plt.bar(x[1:, 0], x[1:, 1])
    plt.plot(x[:, 0], x[:, 1])
    plt.yscale('log')
    plt.show()
    mat = np.array(mat)
    mat = map(mat)
    print('{}:{}'.format(file, mat.shape))
    plt.imshow(mat)
    plt.show()

vis_adj_mat('../../data/adj_matrix_2001_1.npz')