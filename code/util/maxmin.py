import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import fsolve


def f(x):
    return 3*x*x*x-3*x*x-5*x+1

def df(x):
    return 9*x*x-6*x-5

def g(x):
    return 1-0.5*x

def dg(x):
    return -0.5

def get_lambda(x, beta):
    return -g(x)/(2*beta)

def next_x(x0, beta, lr=0.01):
    return x0 + lr*(df(x0) -g(x0)*dg(x0)/(2*beta))

def optimize(xi, beta, lr=0.01, n=20):
    xs = [xi]
    for i in range(n):
        xi = next_x(xi, beta, lr)
        xs.append(xi)

    xs = np.array(xs)
    ys = f(xs)
    return xs, ys

def optimize_adaptive(xi, beta, lr=0.01, n=20):
    xs = [xi]
    for i in range(n):
        xi = next_x(xi, beta, lr)
        xs.append(xi)
        if g(xi)<0:
            beta = g(xi)*dg(xi)*dg(xi) / (2*dg(xi)*df(xi))
            beta -= 1e-4
            if beta < 0:
                beta = 1e-4
            lr = 1e-4

    xs = np.array(xs)
    ys = f(xs)
    return xs, ys

def gradient(x, beta):
    return df(x) - g(x)*dg(x)/(2*beta)

def draw_opt(x0, beta, lr=0.001, n=100, adaptive=False):
    xs = np.arange(-5, 5, 0.1)
    ys_f = f(xs)
    ys_g = g(xs)

    #fig = plt.figure(figsize=(15, 9))
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(13.5, 9), gridspec_kw = {'width_ratios':[1, 1]})
    #ax1 = plt.subplot(121)
    #ax2 = plt.subplot(122)
    ax1.set_xlim([-5, 5])
    ax1.set_ylim([-5, 10])
    ax1.set_aspect('equal', adjustable='box')
    ax1.axhline(y=0, color='k')
    ax1.axvline(x=2, color='r', dashes=[6, 2],)
    ax1.axvspan(-5, 2, alpha=0.1, color='g', label='under-budget')
    ax1.axvspan(2, 5, alpha=0.1, color='r', label='over-budget')

    ax1.set_xlabel(r"$X$")
    ax1.set_ylabel(r"$f(x)$ or $g(x)$")
    ax2.set_xlabel(r"$X$")
    ax2.set_ylabel(r"iteration")
    ax2.set_yscale('symlog', basey=2)

    ax1.set_title('gradient descent on the curve')
    ax2.set_title(r"$x$ at each iteration")

    ax2.axhline(y=2, color='k')
    ax2.set_xlim(-5, 5)
    ax2.set_ylim(0, n)
    ax2.axvspan(-5, 2, alpha=0.1, color='g', label='under-budget')
    ax2.axvspan(2, 5, alpha=0.1, color='r', label='over-budget')

    ax1.plot(xs, ys_f, zorder=0, label=r"$f(x)$")
    ax1.plot(xs, ys_g, zorder=1, label=r"$g(x)$")

    for i in range(len(x0)):
        if adaptive:
            xs_opt, ys_opt = optimize_adaptive(x0[i], beta[i], lr[i], n)
        else:
            xs_opt, ys_opt = optimize(x0[i], beta[i], lr[i], n)
        name = r"$\hat{{x}}_i(x_0:{}-\beta:{}-lr:{})$".format(x0[i], beta[i], lr[i])
        opti, = ax1.plot(xs_opt, ys_opt, label=name, marker='o', markersize=3)

        ts = np.arange(len(xs_opt))

        # gradi = gradient(xs_opt, beta[i])
        # gradi[gradi>3] = 3
        # gradi[gradi<-1] = -1
        # ax2.plot(ts, gradi, zorder=1, dashes=[2, 2], color=opti.get_color(),
        #          label=r"$\frac{{\partial loss}}{{\partial x}}(x_0:{}-\beta:{}-lr:{})$".format(x0[i], beta[i], lr[i]))

        ax2.plot(xs_opt, ts, label=name, marker='o', markersize=3, color=opti.get_color())

        print("beta={}, x*={}".format(beta[i], xs_opt[-1]))

    ax1.legend(loc=2)
    ax2.legend(loc=2)

    if adaptive:
        plt.savefig('../../docs/manuscript/images/opt_demo_adaptive.png', dpi=300, bbox_inches='tight')
    else:
        plt.savefig('../../docs/manuscript/images/opt_demo.png', dpi=300, bbox_inches='tight')

    plt.close()

def draw_grad(beta):
    xs = np.arange(0.5, 3.5, 0.1)
    plt.figure(figsize=(6, 6))
    plt.axhline(y=0, color='k')
    plt.axvline(x=1, color='k')
    plt.axvline(x=1.3, color='k')
    plt.xlim(0.5, 3)
    xs_optim = []
    for betai in beta:
        x_optim = fsolve(lambda x: gradient(x, betai), 2)
        xs_optim.append(x_optim)
        print("beta={}, x*={}".format(betai, x_optim))
        gradi = gradient(xs, betai)
        # gradi[gradi > 100] = 100
        # gradi[gradi < -100] = -100
        plt.plot(xs, gradi, label=r"$\beta={}$".format(betai))
    xs_optim = np.array(xs_optim)
    xs_optim = xs_optim[xs_optim>2]
    plt.plot(xs_optim, [0]*len(xs_optim), 'ro', markersize=5, label='optimum')

    extraticks = [1.3]
    plt.xticks(list(plt.xticks()[0]) + extraticks)

    plt.legend()
    plt.title(r"Gradient of loss with respect to x for differnt $\beta$")
    plt.savefig('../../docs/manuscript/images/opt_grad.png', dpi=300, bbox_inches='tight')
    plt.close()


beta=[10, 0.1, 0.01, 0.005, 0.002, 0.001]
draw_grad(beta)

x0 = [1, 1, 1.3, 1.3, 1.3]
beta = [10, 0.001, 0.005, 0.002, 0.001]
lr = [0.01, 0.01, 0.001, 0.001, 0.001]

draw_opt(x0, beta, lr, 1000, False)
draw_opt(x0, beta, lr, 1000, True)

