import os
from PIL import Image
import numpy as np
import scipy.misc


def mkdirs(paths):
    if isinstance(paths, list) and not isinstance(paths, str):
        for path in paths:
            mkdir(path)
    else:
        mkdir(paths)


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def save_image(image_numpy, image_path):
    image_pil = Image.fromarray(image_numpy)
    image_pil.save(image_path)


# convert from tensor to numpy image
def tensor2im(input_image, dtype=np.uint8):
    return input_image

def resize_image(image_numpy, shape=(512, 512)):
    return scipy.misc.imresize(image_numpy, shape)