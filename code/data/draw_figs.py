import matplotlib.pyplot as plt
import glob, os
import numpy as np


def draw_freq(folder, ai):
    files = glob.glob(folder+'/**/*.npz', recursive=True)
    freqs = []
    for fi in sorted(files):
        freqi = np.load(fi)['freq'][:, :, ai]
        freqs.append(freqi)

    col = 12
    row = len(freqs)//col

    N = row*col
    N0 = len(freqs)
    for i in range(N0, N):
        freqs.append(np.ones_like(freqs[0]))

    maxv = np.max(freqs)
    fig = plt.figure(1, (12*1.5, 7*1.5))
    freqs = np.pad(np.array(freqs), ((0, 0), (1, 1), (1, 1)), 'constant', constant_values=(maxv))
    freqs = np.pad(np.array(freqs), ((0, 0), (2, 2), (2, 2)), 'constant', constant_values=(0))
    freqs = freqs.reshape(row, col, freqs[0].shape[0], freqs[0].shape[1]).transpose(0, 2, 1, 3)
    freqs = freqs.reshape(row*freqs[0].shape[0],-1)
    plt.imshow(freqs)
    plt.savefig(os.path.join(folder,'airline_{}'.format(ai)))
    plt.close()

for i in range(8):
    draw_freq('../../data/raw_features', i)