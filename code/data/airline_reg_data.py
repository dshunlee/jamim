import tensorflow as tf
import numpy as np


class AirlineRegData(object):
    def __init__(self, data_path, n_features, n_airlines, n_airports=60, load_freqency=True, normalization_file=None, route_idx=None):
        self.data_path = data_path
        self.n_features = n_features
        self.n_airlines = n_airlines
        self.n_airports = n_airports
        self.load_freqency = load_freqency
        self.normal_ref = None
        self.optim_route_idx = None
        if route_idx is not None:
            if len(route_idx) > 0:
                self.optim_route_idx = tf.constant(route_idx, dtype=tf.int64)
        if normalization_file:
            self.normal_ref = tf.constant(np.load(normalization_file).astype(np.float32))

    def prepare_data(self):
        dataset = tf.data.TFRecordDataset([self.data_path])
        dataset = dataset.map(lambda x: self._parse_tfrecords(x), num_parallel_calls=4)
        return dataset

    def _parse_tfrecords(self, serialized_example):
        feature = {'train/features': tf.FixedLenFeature((self.n_airlines, self.n_features,), tf.float32),
                   'train/label': tf.FixedLenFeature((self.n_airlines,), tf.float32),
                   'train/route': tf.FixedLenFeature((), tf.int64),
                   'train/coords': tf.FixedLenFeature((2,), tf.int64),
                   'train/freq': tf.FixedLenFeature((), tf.string)}
        features = tf.parse_single_example(serialized_example, features=feature)

        data = features['train/features']
        label = features['train/label']
        freq = tf.decode_compressed(features['train/freq'])
        route = features['train/route']
        coords = features['train/coords']

        if self.optim_route_idx is not None:
            route = tf.gather(self.optim_route_idx, route)

        if self.normal_ref is not None:
            data = data - self.normal_ref[0]
            tf.divide(data, self.normal_ref[1])

        if self.load_freqency:
            freq = tf.py_func(load_freq_from_npy, [freq], tf.float32)
            freq = tf.reshape(freq, (self.n_airlines, self.n_airports, self.n_airports))

        return data, label, route, freq, coords


def load_freq_from_npy(filename):
    fi = filename.decode("utf-8")
    freq = np.load(fi)['freq'].astype(np.float32).transpose(2, 0, 1)
    return freq

