import glob, os
import pandas as pd
import scipy.sparse as sparse
import pickle
import numpy as np


def gen_classic_features(args):
    # fields to load for on_time performance
    fields = ['Year', 'Month', 'OriginAirportSeqID', 'DestAirportSeqID',
              'Carrier', 'DepDelay', 'DepDelayMinutes', 'Cancelled', 'Diverted']
    airline_df = read_data_to_frame(fields, args.input)
    airports = load_all_airports(args.airportfile)
    airline_df = add_id_to_frame(airports, airline_df)
    on_time_performance = compute_on_time_performance(airline_df)

    # fields to load for ticket price
    t_price_fields = ['Year', 'Quarter', 'OriginAirportSeqID',
                      'RPCarrier', 'FarePerMile']
    # filenames = 'airline/Origin_and_Destination_Survey_DB1BTicket_2001*'
    t_price_df = read_data_to_frame(t_price_fields, args.ticketfile)
    t_price_df['OriginID'] = t_price_df['OriginAirportSeqID'].map(airports)
    save_classical_features(args.output, args.carrierfile, on_time_performance, t_price_df, len(airports))
    # save_quarterly_features(args.output,  len(airports))


def read_data_to_frame(fields, source_dir='airline/On_Time_On_Time_Performance_2000_*'):
    all_rec = glob.iglob(source_dir, recursive=True)
    dataframes = (pd.read_csv(f, usecols=fields) for f in all_rec)
    df = pd.concat(dataframes, ignore_index=True)
    return df


def load_all_airports(filename='all_airports.pkl'):
    with open(filename, 'rb') as f:
        return pickle.load(f)


def add_id_to_frame(airports_dict, airline_df):
    # map AirportSeqID to unique IDs from 1 to n
    # airports = pd.unique(airline_df[['OriginAirportSeqID', 'DestAirportSeqID']].values.ravel())
    # airports_dict = dict([(y, x + 1) for x, y in enumerate(sorted(set(airports)))])

    # add unique ID (1 to n) to each airport
    airline_df['OriginID'] = airline_df['OriginAirportSeqID'].map(airports_dict)
    airline_df['DestID'] = airline_df['DestAirportSeqID'].map(airports_dict)

    return airline_df.drop(['OriginAirportSeqID', 'DestAirportSeqID'], axis=1)


def compute_on_time_performance(airline_df):
    index = ['Year', 'Month', 'OriginID', 'DestID', 'Carrier']
    grouped_performance = airline_df.groupby(index).apply(group_carriers).reset_index()

    index = ['Year', 'Month', 'OriginID', 'DestID']
    market_share = pd.DataFrame({'marketshare':
                                     airline_df.groupby(index).Carrier.value_counts(normalize=True)}).reset_index()
    merged_df = pd.merge(grouped_performance, market_share,
                         on=['Year', 'Month', 'OriginID', 'DestID', 'Carrier'],
                         how='outer')
    return merged_df


def group_carriers(x):
    df = {}
    df['average_delay'] = x['DepDelayMinutes'].mean()
    df['cancel_ratio'] = x['Cancelled'].mean()
    df['delay_ratio'] = x['DepDelayMinutes'].astype(bool).sum() / x['Carrier'].count()
    df['divert_ratio'] = x['Diverted'].mean()
    df['frequency'] = x['Carrier'].count()
    return pd.Series(df)


def save_classical_features(outputdir, airportfile, on_time_performance, t_price_df, airports_no):
    if not os.path.exists(outputdir):
        os.mkdir(outputdir)
    month_list = pd.unique(on_time_performance['Month'].values)  # four quarters per year
    year_list = pd.unique(on_time_performance['Year'].values)  # get year range from data
    carrier_list = pd.unique(on_time_performance['Carrier'].values)
    carriers = load_all_airports(airportfile)

    for year in year_list:
        for month in month_list:
            average_delay = np.zeros((airports_no, airports_no, len(carriers)))
            delay_ratio = np.zeros((airports_no, airports_no, len(carriers)))
            cancel = np.zeros((airports_no, airports_no, len(carriers)))
            diverted = np.zeros((airports_no, airports_no, len(carriers)))
            frequency = np.zeros((airports_no, airports_no, len(carriers)))
            market = np.zeros((airports_no, airports_no, len(carriers)))
            t_price = np.zeros((airports_no, len(carriers)))

            for carrier in carrier_list:
                df = on_time_performance[(on_time_performance['Month'] == month)
                                         & (on_time_performance['Year'] == year) &
                                         (on_time_performance['Carrier'] == carrier)]
                carrierID = carriers[carrier]
                quarter = (month - 1) // 3 + 1
                price_df = t_price_df[(t_price_df['Quarter'] == quarter)
                                      & (t_price_df['Year'] == year) &
                                      (t_price_df['RPCarrier'] == carrier)]
                rows = np.zeros(len(price_df['OriginID']))
                average_delay_matrix = sparse.coo_matrix((df['average_delay'], (df['OriginID'], df['DestID'])),
                                                         shape=(airports_no, airports_no))
                average_delay[:,:, carrierID] = average_delay_matrix.todense()
                delay_ratio_matrix = sparse.coo_matrix((df['delay_ratio'], (df['OriginID'], df['DestID'])),
                                                       shape=(airports_no, airports_no))
                delay_ratio[:, :, carrierID] = delay_ratio_matrix.todense()
                cancel_matrix = sparse.coo_matrix((df['cancel_ratio'], (df['OriginID'], df['DestID'])),
                                                  shape=(airports_no, airports_no))
                cancel[:, :, carrierID] = cancel_matrix.todense()
                diverted_matrix = sparse.coo_matrix((df['divert_ratio'], (df['OriginID'], df['DestID'])),
                                                    shape=(airports_no, airports_no))
                diverted[:, :, carrierID] = diverted_matrix.todense()
                frequency_matrix = sparse.coo_matrix((df['frequency'], (df['OriginID'], df['DestID'])),
                                                     shape=(airports_no, airports_no))
                frequency[:, :, carrierID] = frequency_matrix.todense()
                market_matrix = sparse.coo_matrix((df['marketshare'], (df['OriginID'], df['DestID'])),
                                                  shape=(airports_no, airports_no))
                market[:, :, carrierID] = market_matrix.todense()

                t_price_vector = sparse.coo_matrix((price_df['FarePerMile'], (price_df['OriginID'], rows)),
                                                   shape=(airports_no, 1))
                t_price[:, carrierID] = t_price_vector.toarray().reshape(airports_no)
            filename = outputdir + "features_" + str(year) + "_" + str(month) + ".npz"
            f_handle = open(filename, 'wb')
            np.savez(f_handle,
                     avg_delay=average_delay,
                     delay_ratio=delay_ratio,
                     cancel_ratio=cancel,
                     divert_ratio=diverted,
                     freq=frequency,
                     t_price=t_price,
                     mrktshare=market)
            f_handle.close()


def save_quarterly_features(outputdir, quarterly_data, airports_no):
    if not os.path.exists(outputdir):
        os.mkdir(outputdir)
    quarter_list = pd.unique(quarterly_data['Quarter'].values)  # four quarters per year
    year_list = pd.unique(quarterly_data['Year'].values)  # get year range from data
    carrier_list = pd.unique(quarterly_data['RPCarrier'].values)
    for year in year_list:
        for quarter in quarter_list:
            for carrier in carrier_list:
                df = quarterly_data[(quarterly_data['Quarter'] == quarter)
                                    & (quarterly_data['Year'] == year) &
                                    (quarterly_data['RPCarrier'] == carrier)]

                rows = np.zeros(len(df['OriginID']))
                t_price_matrix = sparse.coo_matrix((df['FarePerMile'], (rows, df['OriginID'])),
                                                   shape=(1, airports_no))
                for i in (0, 2):
                    filename = outputdir + str(carrier).lower() + "_features_" + str(year).lower() + "_" \
                               + str(i + quarter).lower() + ".npz"
                    f_handle = open(filename, 'ab')
                    np.savez(f_handle, t_price=t_price_matrix)
                    f_handle.close()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='generate on time performance for routs')

    parser.add_argument('--input', '-i', type=str, default='../../data/On_Time_On_Time_Performance_2000_1.*',
                        help='input folder')
    parser.add_argument('--ticketfile', '-t', type=str,
                        default='../../data/Origin_and_Destination_Survey_DB1BTicket_2000_1.*',
                        help='input ticketprice file')
    parser.add_argument('--output', '-o', type=str, default='../../data/',
                        help='path to output folder')
    parser.add_argument('--airportfile', '-f', type=str, default='../../data/all_airports_10years.pkl',
                        help='file has all airports')
    parser.add_argument('--carrierfile', '-c', type=str, default='../../data/all_carriers_10years.pkl',
                        help='file has all carriers')

    args = parser.parse_args()
    gen_classic_features(args)

# on_time_performance = read_data_to_frame('Airline/On_Time_On_Time_Performance_2000_1.csv.gz')
# airports = load_all_airports('all_airports.pkl')
# on_time_performance = add_id_to_frame(airports, on_time_performance)

# test = pd.read_csv('airline/PCE_all.csv.gz')

# print(on_time_performance.head())
