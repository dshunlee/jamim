import glob, os
import pandas as pd
import scipy.sparse as sparse
from sklearn.preprocessing import normalize


def gen_adj_mat(args):
    # Read data
    airline_df = read_data_to_frame(args.input)
    airline_df = add_id_to_frame(airline_df)
    #print(airline_df.head())
    weighted_edge_collection = get_edge_collection(airline_df)
    A_collection = adj_mat_from_graph_edges(weighted_edge_collection)

    if args.normalize:
        A_collection = normalize_adjs(A_collection)

    save_adj_mat(args.output, A_collection)


def read_data_to_frame(source_dir):
    all_rec = glob.iglob(source_dir, recursive=True)
    fields = ['OriginAirportSeqID', 'DestAirportSeqID', 'Year', 'Quarter']
    dataframes = (pd.read_csv(f, usecols = fields) for f in all_rec)
    df = pd.concat(dataframes, ignore_index=True)
    return df


def add_id_to_frame(airline_df):
    # map AirportSeqID to unique IDs from 1 to n
    airports = pd.unique(airline_df[['OriginAirportSeqID', 'DestAirportSeqID']].values.ravel())
    airports_dict = dict([(y, x + 1) for x, y in enumerate(sorted(set(airports)))])

    # add unique ID (1 to n) to each airport
    airline_df['OriginID'] = airline_df['OriginAirportSeqID'].map(airports_dict)
    airline_df['DestID'] = airline_df['DestAirportSeqID'].map(airports_dict)

    return airline_df


def get_weighted_edgelist(airline_df):
    #get weighted edgelist from data
    weighted_edgelist = pd.DataFrame({'Weights':
                                  airline_df.groupby(['OriginID', 'DestID']).size()}).reset_index()
    return weighted_edgelist


def get_edge_collection(airline_df):
    # create a dictionary to hold weighted_edge dataframe for each quarter
    weighted_edge_collection = {}
    quarter_list = pd.unique(airline_df['Quarter'].values)  # four quarters per year
    year_list = pd.unique(airline_df['Year'].values)  # get year range from data
    for year in year_list:
        for quarter in quarter_list:
            key = str(year) + "q" + str(quarter)
            weighted_edge_collection[key] = get_weighted_edgelist(
                airline_df[(airline_df["Year"] == year) & (airline_df["Quarter"] == quarter)])

    return weighted_edge_collection


def adj_mat_from_graph_edges(weighted_edge_collection):
    A_collection = {}
    for key in weighted_edge_collection.keys():
        A_collection[key] = sparse.coo_matrix((weighted_edge_collection[key]['Weights'],
                             (weighted_edge_collection[key]['OriginID'], weighted_edge_collection[key]['DestID'])))
    return A_collection

def normalize_adjs(A_collection):
    for key in A_collection.keys():
        # normalize adjacency matrix by column (axis = 0)
        A_collection[key] = normalize(A_collection[key], norm='l1', axis=0)
    return A_collection

def save_adj_mat(outputdir, A_collection):

    if not os.path.exists(outputdir):
        os.mkdir(outputdir)
    i = 1
    for key in A_collection.keys():
        i = i + 1
        name = 'adj_matrix_' + str(key) + '.npz'
        sparse.save_npz(os.path.join(outputdir, name), A_collection[key])


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='data preparing.')
    parser.add_argument('--input', '-i', type=str, default='../../data/example_2002', help='input folder')
    parser.add_argument('--output', '-o', type=str, default='../../data/adjmatrix/',
                        help='path to output folder')

    parser.add_argument('--normalize', '-n', action='store_true', help='normalize the adjacency matrix')

    args = parser.parse_args()
    gen_adj_mat(args)




