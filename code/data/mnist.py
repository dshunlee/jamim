import tensorflow as tf
import numpy as np
import sys, os
import argparse
import scipy
import scipy.sparse as sp
from tensorflow.contrib.data.python.ops import batching
from tensorflow.contrib.data.python.ops import grouping

if __name__ == "__main__":
    from data_util import _float_feature, _int64_feature
else:
    from .data_util import _float_feature, _int64_feature

class MNISTData(object):
    def __init__(self, data_path):
        self.data_path = data_path

    def prepare_data(self):
        dataset = tf.data.TFRecordDataset([self.data_path])
        dataset = dataset.map(lambda x: self._parse_tfrecords(x), num_parallel_calls=4)
        return dataset

    def _parse_tfrecords(self, serialized_example):
        feature = {'train/img': tf.FixedLenFeature((28, 28), tf.float32)}
        features = tf.parse_single_example(serialized_example, features=feature)
        img = features['train/img']
        return img


# convert dense adjacency matrix to tfrecords
def mnist_to_tfrecords(output_name='mnist-train.tfrecords'):
    # open the TFRecords file
    from keras.datasets import mnist
    import numpy as np

    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train = np.array(x_train, dtype=np.float32) / 255.0
    y_train = np.array(y_train, dtype=np.int64).reshape(-1, 1)
    x_test = np.array(x_test, dtype=np.float32) / 255.0
    y_test = np.array(y_test, dtype=np.int64).reshape(-1, 1)

    def totfrecords(x, y, outputfile):
        writer = tf.python_io.TFRecordWriter(outputfile)
        for i in range(x.shape[0]):
            # Create a feature
            feature = {'train/img': _float_feature(x[i].flatten()),
                       'train/label': _int64_feature(y[i])}
            # Create an example protocol buffer
            example = tf.train.Example(features=tf.train.Features(feature=feature))

            # Serialize to string and write on the file
            writer.write(example.SerializeToString())
        writer.close()

    totfrecords(x_train, y_train, output_name)
    totfrecords(x_test, y_test, output_name.replace('train', 'val'))
    totfrecords(x_test, y_test, output_name.replace('train', 'test'))


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    mnist_to_tfrecords('./data/mnist-train.tfrecords')

    with tf.Session() as sess:
        ego = MNISTData('./data/mnist-train.tfrecords')
        dataset = ego.prepare_data()
        dataset = dataset.shuffle(buffer_size=20).repeat().batch(1)
        #dataset = dataset.window(4)
        #dataset = dataset.apply(grouping._map_x_dataset(window_pad_batch))
        # Initialize all global and local variables
        init = tf.global_variables_initializer()
        sess.run(init)

        batchi_dense = dataset.make_one_shot_iterator().get_next()
        for i in range(20):
            xx = sess.run(batchi_dense)
            #value = sess.run(dx, feed_dict={dx: xx})
            plt.imshow(xx[0])
            plt.pause(0.1)

        sess.close()

