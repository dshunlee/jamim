import numpy as np
from scipy import sparse


def compute_pagerank(a, beta=0.85, epsilon=10 ** -4):
    # Efficient computation of the PageRank values using a sparse adjacency
    # matrix and the iterative power method.
    #
    # Parameters
    # ----------
    # a : weighted adjacency matrix. weighted by flight frequency.
    # beta: 1-teleportation probability, to handle dangling nodes (no outgoing link)
    # epsilon: stop condition. Minimum allowed amount of change in the PageRanks
    #     between iterations.
    #
    # Returns
    # -------
    # output :
    #     normalized PageRank array, sum equals to one.

    # Test adjacency matrix is OK
    n, _ = a.shape
    assert (a.shape == (n, n))
    # Constants Speed-UP, also to normalize A by column
    deg_out_beta = a.sum(axis=0).T / beta  # vector
    # Initialize
    ranks = np.ones((n, 1)) / n  # vector
    time = 0
    flag = True
    while flag:
        time += 1
        with np.errstate(divide='ignore'):  # Ignore division by 0 on ranks/deg_out_beta
            new_ranks = a.dot((ranks / deg_out_beta))  # vector
        # Leaked PageRank
        new_ranks += (1 - new_ranks.sum()) / n
        # Stop condition
        if np.linalg.norm(ranks - new_ranks, ord=1) <= epsilon:
            flag = False
        ranks = new_ranks
    return ranks


def compute_ego_density(a):
    # a is a coo_matrix, numbers are actual flight frequency.
    edge_weights_sum = a.sum(axis=0).T + a.sum(axis=1)

    # calculate no. of connected airports (including in/column and out/row)
    b = a.tocsr() + a.transpose().tocsr()
    vertx_count = np.diff(b.indptr) #count non-zero numbers per row, exclude itself

    vertx_count = [(item * (item + 1)) for item in vertx_count] # total possible connections

    with np.errstate(divide='ignore'):  # Ignore division by 0
        density = np.true_divide(edge_weights_sum, vertx_count)  # vector
    return density.T


def compute_centrality(a):
    # a is a coo_matrix, numbers are actual flight frequency.
    out_degree_centrality = a.sum(axis=1).T
    in_degree_centrality = a.sum(axis=0).T

    return out_degree_centrality, in_degree_centrality


def compute_network_features(index, data, shape):
    a = sparse.coo_matrix((data, index), shape)
    pagerank = compute_pagerank(a)
    ego_density = compute_ego_density(a)
    out_degree_centrality, in_degree_centrality = compute_centrality(a)

    return pagerank, ego_density, out_degree_centrality, in_degree_centrality



# a = sparse.load_npz('./data/adj_matrix_2001_1.npz')
#
# pagerank, ego_density, out_degree_centrality, in_degree_centrality = compute_network_features()
# print("ranks:", pagerank)
