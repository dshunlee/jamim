import tensorflow as tf
import numpy as np
import sys, os
import argparse
import scipy
import scipy.sparse as sp
from tensorflow.contrib.data.python.ops import batching
from tensorflow.contrib.data.python.ops import grouping

if __name__ == "__main__":
    from data_util import _float_feature, _int64_feature
else:
    from .data_util import _float_feature, _int64_feature

class EgoAdjMatrix(object):
    def __init__(self, data_path, n_airport=60, max_Freq=1, max_dFreq=0.01, min_dFreq=0.001,
                 aug_noise=0.5, aug_new_edges=0.25, aug_new_perc=0.05, use_ego=False):
        self.data_path = data_path
        self.n_airport = n_airport
        self.max_Freq = max_Freq
        self.max_dFreq = max_dFreq
        self.min_dFreq = min_dFreq
        self.aug_noise = aug_noise
        self.aug_new_edges = aug_new_edges
        self.aug_new_perc = aug_new_perc
        self.use_ego = use_ego

    def prepare_data(self):
        dataset = tf.data.TFRecordDataset([self.data_path])
        dataset = dataset.map(lambda x: self._parse_tfrecords(x), num_parallel_calls=4)
        return dataset

    # data normalization
    def _normalize(self, data):
        # normalize the freq to [0, 1]
        return data/self.max_Freq

    # data augmentation
    # assume data has been normalized between 0 to 1
    def _augment(self, data):
        # add noise to existing
        if np.random.rand(1) < self.aug_noise:
            data *= 1 + tf.random_uniform(tf.shape(data), self.min_dFreq, self.max_dFreq)
        # add new edges
        if np.random.rand(1) < self.aug_new_edges:
            # for new edges, add at most two times the minimum to the matrix
            mask = (tf.to_float(data == 0) * tf.random_uniform(tf.shape(data))) < self.aug_new_perc
            data += tf.to_float(mask) * tf.random_uniform(tf.shape(data), 0, 2 * tf.reduce_min(data))
        return data

    # get the ego matrix, for the id th airport
    @staticmethod
    def _get_ego(data):
        data += tf.eye(tf.shape(data)[1])
        # sum>0 the node is linked with other nodes
        sum = tf.reduce_sum(data, 0) + tf.reduce_sum(data, 1)
        # ramdom sampling, sum>2(two diagnal 1)
        mask = tf.to_float(sum > 2) * tf.random_uniform(tf.shape(sum))
        # randomly select the one with highest possibility
        id = tf.argmax(mask)
        # build ego-network
        idx = tf.logical_or(data[id, :] > 0, data[:, id] > 0)
        data -= tf.eye(tf.shape(data)[1])
        mask = tf.logical_and(tf.expand_dims(idx, -1), idx)
        ego = data * tf.to_float(mask)
        #ego = tf.matrix_set_diag(ego, tf.zeros(ego.shape[0]))
        #tfprint = tf.print('sum: ', sum[id], "sum_ego: ", tf.reduce_sum(ego))
        #with tf.control_dependencies([tfprint]):
        return ego
        #return data, ego, idx, id, mask

    def _parse_tfrecords(self, serialized_example):
        feature = {'train/adj_shape': tf.FixedLenFeature((2,), tf.int64),
                   'train/adj_matrix': tf.VarLenFeature(tf.float32),
                   }
        features = tf.parse_single_example(serialized_example, features=feature)
        shape = tf.constant([self.n_airport, self.n_airport])

        adj_mat = tf.reshape(tf.sparse_tensor_to_dense(features['train/adj_matrix'], default_value=0), shape)

        if self.max_Freq:
            adj_mat = self._normalize(adj_mat)

        adj_mat = self._augment(adj_mat)

        if self.use_ego:
            adj_mat = self._get_ego(adj_mat)

        return adj_mat


# convert dense adjacency matrix to tfrecords
def adj_mat_to_tfrecords(input_files, output_name='train.tfrecords', keep_zero_ratio=0.05):
    # open the TFRecords file
    writer = tf.python_io.TFRecordWriter(output_name)
    maxfreq = 0.0
    for i in range(len(input_files)):
        # print how many images are saved every 1000 images
        if not i % 1000:
            print('Train data: {}/{}'.format(i, len(input_files)))
            sys.stdout.flush()
        # Load the image
        adj_mat = load_adj_mat(input_files[i])
        if np.ndim(adj_mat) == 2:
            adj_mat = np.expand_dims(adj_mat, axis=-1)
        else:
            adj_mat = adj_mat.transpose(2, 0, 1)
        #adj_mat = adj_mat[:1]
        for adj in adj_mat:
            #if np.sum(adj) == 0:
            #    continue
            maxfreq = np.max((np.max(adj), maxfreq))
            # Create a feature
            feature = {'train/adj_shape': _int64_feature(adj.shape),
                       'train/adj_matrix': _float_feature(adj.flatten())}
            # Create an example protocol buffer
            example = tf.train.Example(features=tf.train.Features(feature=feature))

            # Serialize to string and write on the file
            writer.write(example.SerializeToString())

    writer.close()
    sys.stdout.flush()
    print("maxfreq: {}".format(maxfreq))

    return maxfreq


def load_adj_mat(filename):
    ext = os.path.splitext(filename)[1]
    data = np.load(filename)
    if ext == '.npy':
        return data
    else:
        try:
            return data['freq']
        except Exception as e:
            print(e)


def convert(args):
    if not os.path.isfile(args.input):
        raise IOError(errno.ENOENT, 'Not a file', args.input)
    filelist = []
    base_dir = os.path.dirname(args.input)
    with open(args.input) as fin:
        str = fin.readline().strip()
        while str:
            filelist.append(os.path.join(base_dir, str))
            str = fin.readline().strip()

    adj_mat_to_tfrecords(filelist, args.output)

def window_pad_batch(dataset):
    return batching.padded_batch_window(dataset, [-1, -1])


def check_data(args):
    import matplotlib.pyplot as plt
    with tf.Session() as sess:
        ego = EgoAdjMatrix(args.output, 60, args.maxfreq, 0.01, 0.001, use_ego=True)
        dataset = ego.prepare_data()
        dataset = dataset.batch(1)
        #dataset = dataset.window(4)
        #dataset = dataset.apply(grouping._map_x_dataset(window_pad_batch))
        # Initialize all global and local variables
        init = tf.global_variables_initializer()
        sess.run(init)

        batchi_dense = dataset.make_one_shot_iterator().get_next()

        fig, ax = plt.subplots(2, 2)
        for i in range(20):
            xx = sess.run(batchi_dense)
            #value = sess.run(dx, feed_dict={dx: xx})
            mask = np.logical_and(xx[2][0].reshape(-1, 1), xx[2][0])
            ax[0, 0].imshow(xx[0][0])
            ax[0, 1].imshow(mask)
            ax[1, 0].imshow(xx[1][0])
            ax[1, 1].imshow(xx[0][0]*mask)
            plt.show()
            print(xx[3][0])
            #plt.pause(0.1)

        sess.close()


if __name__ == "__main__":
    import errno
    parser = argparse.ArgumentParser(description='data preparing.')
    parser.add_argument('what', type=str, help='convert or test')
    parser.add_argument('--input', '-i', type=str, default='../../data/freq_8carriers/train/filelist.txt', help='path to npz file list')
    parser.add_argument('--output', '-o', type=str, default='../../data/freq_8carriers/train_a60.tfrecords', help='path to output tfrecords file')
    parser.add_argument('--maxfreq', type=int, default=1500, help='sparse matrix or not')

    args = parser.parse_args()

    convert(args)
    check_data(args)
