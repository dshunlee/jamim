import tensorflow as tf
from options.test_options import TestOptions
from data.airline_reg_data import AirlineRegData
from models.jamim_regression_model import JamimRegressionModel
import numpy as np
import os
import models.evaluate as eval
import pickle

import sys
sys.path.append('../../../')


def test(opt):
    data_loader = AirlineRegData(opt.dataroot, opt.n_features, opt.n_airlines, opt.normarlize_file)
    dataset = data_loader.prepare_data()
    dataset = dataset.batch(opt.batchSize, drop_remainder=True)

    iterator = dataset.make_one_shot_iterator()
    next_batch = iterator.get_next()
    dataset_init_op = iterator.make_initializer(dataset, name='dataset_init')

    jamim = JamimRegressionModel()
    jamim.initialization(opt)
    jamim.custom_network(next_batch)

    saver = tf.train.Saver()

    model_ckpt = opt.ckpt
    if model_ckpt is None:
        model_ckpt = os.path.join(jamim.save_dir, "latest.ckpt")

    pred_rts = {}
    label_rts = {}

    with tf.Session() as sess:
        saver.restore(sess, model_ckpt)
        sess.run(dataset_init_op)
        w, b = sess.run([jamim._w, jamim._b])
        with open('w_b.pkl', 'wb') as fout:
            pickle.dump(w, fout)
            pickle.dump(b, fout)
        print('w shape:{}, b shape:{}'.format(w.shape, b.shape))
        while True:
            try:
                values = sess.run([jamim.pred, next_batch])

                pred = values[0]
                label = values[1][1]
                route = values[1][2]
                # print(route)
                for i in range(opt.batchSize):
                    rt = route[i]
                    if rt in pred_rts.keys():
                        pred_rts[rt].append(pred[i])
                        label_rts[rt].append(label[i])
                    else:
                        pred_rts[rt] = [pred[i]]
                        label_rts[rt] = [label[i]]
            except tf.errors.OutOfRangeError:
                break

        # r2, cc = eval.evl_r2_pearson(pred_rts, label_rts)
        r2, cc, mse = eval.evl_r2_pearson(pred_rts, label_rts)
        print("R2: {}, pearson cc: {}".format(r2, cc))
        print("mse: {}".format(mse))


if __name__ == '__main__':
    opt = TestOptions().parse()
    test(opt)