import tensorflow as tf
from options.train_options import TrainOptions
from data.ego_adj_mat import EgoAdjMatrix
from models.ego_autoencoder_model import EgoAutoencoderModel
import models.train_utils as train_utils
from util.visualizer import Visualizer

import sys, os
sys.path.append('../../../')


def train(opt):
    # prepare data
    train_dataset_one_epoch, train_dataset, val_dataset, test_dataset = prepare_data(opt)

    # train_dataset_one_epoch, train_dataset, val_dataset, test_dataset = prepare_mnist_data(opt)
    # opt.n_airports = 28

    # create model
    model = EgoAutoencoderModel()

    # create visualizer
    visualizer = Visualizer(opt)

    train_utils.train(opt, model, train_dataset, val_dataset, test_dataset, train_dataset_one_epoch, visualizer)


def prepare_data(opt):
    train_data_loader = EgoAdjMatrix(opt.dataroot, opt.n_airports, opt.max_Freq, opt.max_dFreq, opt.min_dFreq, use_ego=opt.use_ego)
    train_dataset = train_data_loader.prepare_data()
    train_dataset_one_epoch = train_dataset.shuffle(buffer_size=2048).batch(opt.batchSize)
    train_dataset = train_dataset.repeat().shuffle(buffer_size=2048).batch(opt.batchSize)

    val_data_loader = EgoAdjMatrix(opt.dataroot.replace('train', 'val'), opt.n_airports,
                                   opt.max_Freq, opt.max_dFreq, opt.min_dFreq, use_ego=opt.use_ego)
    val_dataset = val_data_loader.prepare_data().batch(opt.batchSize)

    test_data_loader = EgoAdjMatrix(opt.dataroot.replace('train', 'test'), opt.n_airports,
                                    opt.max_Freq, opt.max_dFreq, opt.min_dFreq, use_ego=opt.use_ego)
    test_dataset = test_data_loader.prepare_data().batch(opt.batchSize)

    return train_dataset_one_epoch, train_dataset, val_dataset, test_dataset


def prepare_mnist_data(opt):
    from data.mnist import MNISTData
    opt.data_root = './data/mnist-train.tfrecords'
    train_data_loader = MNISTData(opt.data_root)
    train_dataset = train_data_loader.prepare_data()
    train_dataset_one_epoch = train_dataset.shuffle(buffer_size=2048).batch(opt.batchSize)
    train_dataset = train_dataset.repeat().shuffle(buffer_size=2048).batch(opt.batchSize)

    val_data_loader = MNISTData(opt.data_root.replace('train', 'val'))
    val_dataset = val_data_loader.prepare_data().batch(opt.batchSize)

    test_data_loader = MNISTData(opt.data_root.replace('train', 'test'))
    test_dataset = test_data_loader.prepare_data().batch(opt.batchSize)

    return train_dataset_one_epoch, train_dataset, val_dataset, test_dataset


if __name__ == '__main__':
    opt = TrainOptions().parse()
    #train_orig_gcn(opt)
    train(opt)
