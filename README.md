# JAMIM (Joint Airline Market Influnce Maximization)

## Data preparation
To convert adjacency matrix npy files (for autoencoder) listed in `train.txt` to tfrecords format, run

    python ego_adj_mat.py convert -s \
    -i ../../dadta/train.txt \
    -o ../../data/train_autoencoder.tfrecords

To convert features npz files (for regression) listed in `train.txt` to tfrecords format, run

    python ./code/prep_jamim_data.py convert \
    -i data/features/train/filelist.txt \
    -o data/features/a60f8_train.tfrecords \
    -a data/top_10_route.npz

'-a' points to the sparse matrix about specifying routes you want to do regression.


## Training
Requirements:

Visdom is required for real time visualization of the training process.

    pip install visdom

To visualize the training process of the regressor, run the following script to open a port,
 or use the `creat_ports.sh` to create multiple ports:

    python3 -m visdom.server -port 8099
    
To train the autoencoder, ran the following command line. 

    python code/train_autoencoder.py --lr_policy exp \
    --encoder_hidden 128 64 32 --max_Freq 1000 --aemodel mlp \
    --n_airports 60 --name autoencoder \
    --dataroot ./data/freq_8carriers/ae_train.tfrecords \
    --model ego_autoencoder --n_epoch 5000 --batchSize 64  --lr 0.0001 \
    --suffix {encoder_hidden} --print_freq 6 --display_freq 50

To prepare the data:

    python code/prep_jamim_data.py convert -a './data/r10.npz' \
    -i ./data/features/train/filelist.txt \
    -o './data/features/a60f8_train.tfrecords' \
    -cd './data/cost_demand.npz' 


To train the regression model, ran the following command line.

    python code/train_regressor.py --use_graph_feat --fc_nfeats 3 3 \
    --use_autoencoder --aemodel mlp --encoder_hidden 128 64 32 \
    --n_routes 3000 --n_features 6 --batchSize 64 --n_epoch 200 --display_port 8099 \
    --model jamim_regression --name a60f8 --dataroot ./data/features/a60f8_train.tfrecords \
    --lr 0.0001 --lr_policy exp --print_freq 10 \
    --pretrained_weights "./checkpoints/autoencoder_128+64+32/latest.ckpt"

+ `--what train` set model to training mode.
+ `--model jamim_regression` specifies the model.
+ `--n_airlines [int]` is the total number of airlines.
+ `--n_routes [int]` is the number of routes.
+ `--n_features [int]` is the total number of classical features.

One list of integers is used to configure FC layers:

+ `--fc_nfeats [int list]` is the number of kernels in the FC layer, default empty.

The following configurations are used to control the loss.

+ `--loss_weight_method` is the loss weighting method, choose between `zeroout`,`freq`,`weight`; default `zeroout`. 

    + `zeroout` considers only non-zero influence; 
    + `freq` weights the influence with frequency; 
    + `weight` give a fixed weight to non-zero influence.
        + `--loss_weight` specifies the fixed weight. 
        
+ `--freq_mask_file` specifies the npz file with ['freq', 'mask'] for frequency and mask.
The freq will be normalized by the maximum.
If this file is not specified, the algorithm uses batch freq for `freq` weight with `max_Freq` for normlize; 
and the value of batch ground truth for `zeroout`. 

Two switches are used to control which feature should be include：

+ `--use_graph_feat [switch]` will add graph features.
+ `--use_autoendoer [switch]` will add autoencoder features.

If autoencoder is used, the following should be configured based on the pretrained model

+ `--aemodel [str]` is the autoencoder model.
+ `--max_Freq [float]` is used to normalize the frequencies in autoencoder. 
+ `--encoder_hidden [int list]` specifies the configuration of the autoencoder.
+ `--pretrained_weights [str]` specifies the location of the pretrained autoencoder.

Graph feature and autoencoder feature can be precomputed.
To do this, add a txt file that lists all of the feature files, including train/val/test.

+ `--freq_files [str]` is a txt file that lists all of the feature files.

## Optimization

During optimization, `pretrained_weights` locates to the pretrained regression model; 
the default setting will try to find the weights in the same folder.
There are also extra arguments needs to be passed:

+ `--what optimize` set model to optimize mode.
+ `--init_freq_file [str]` is the initial frequency file of all airlines.
+ `--opt_airline [int]` is the id of the airline you want to optimize, start from 0. 
+ `--init_freq [switch]` inits the freq with current values, otherwise random initialize to values close to zero.
+ `--pretrained_weights [str]` specifies the location of the pretrained regression model, can be ignored.
+ `--cost_demand [str]` specifies the location of the cost (C) and demand (D) data, npz file.
+ `--cost_scale [float]` scale (cs) the cost to avoid large numbers, this will also be applied to the budget.
+ `--demand_scale [float]` scale (ds) the cost to avoid large numbers.
+ `--over_budget_penalty [float]` is the beta that penalize cost over run, default 1.
+ `--budget_penalty_beta0 [float]` is multiplier of beta that penalize cost over run to ensure convergence, 
0.9 - 1000 works fine, the larger the faster.
+ `--total_budget [float]` is the total budget (B), if not given it is the existing total cost.
+ `--budget_normalize [switch]` normalize the cost by the total budget.
+ `--jamim_lr_decay [float]`, learning rate decay for optimization, 0.9 works fine. **Must put `lr_policy` to `custom`**. 
**If it is set to 0.0, the learning rate is decayed by the cost over budget.**
+ `--limit_max_freq [switch]`, limit maximum freq to existing maximum
+ `--inverse_img [switch]`, inverse the image

The objective is to minimize:

        losss = - sum( influence ) + over_budget_penalty * relu( loss_over_budget )
where

    loss_over_budget = ( sum( (cs * C) * Freq ) - B ) / B
    influence = f(Freq) * (ds * D)
    
During optimization, following hyperparameters should be tuned:

+ `--cost_scale` scales the cost.
+ `--demand_scale` scales the influence.
+ `--lr` can be increased to speed up optimization.
+ `--lr_policy` can be set to fix or exp. 

**Note**: the batchsize for optimization should be the number of routes in the regression model.

### Baseline 

To run baseline, add the following options:

+ `--baseline [str]` choose between [greedy | greedy_loop | group_greedy | brutal]. 
`brutal` is for brutal search for small problems. `greedy_loop` is the tensorflow while loop version of `greedy`.
+ `--gr_step [int]` is the step of each updates, default 10.  
  



